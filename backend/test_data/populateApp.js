const Teacher = new require('../models/Teacher');
const Student = new require('../models/Student');
const Parent = new require('../models/Parent');
const ClassBook = new require('../models/ClassBook');
const Subject = new require('../models/Subject');

const bcrypt = require('bcrypt');
const saltRounds = 2;

const admin = require('../config/admin.config.json');

/*TEST TEACHERS*/
let teachers = [
    {
        firstName: 'Mujo',
        lastName: 'Mujic',
        address: 'Falovska 15',
        birthdate: new Date(1970, 10, 15),
        umcn: 2467776095983,
        sex: 'M',
        phone: 38761555666,
        username: 'mmujic1',
        password: 'mujopass'
    },
    {
        firstName: 'Suljo',
        lastName: 'Suljic',
        address: 'Orlovacka 11',
        birthdate: new Date(1969, 8, 13),
        umcn: 7943719051515,
        sex: 'M',
        phone: 38761999333,
        username: 'ssuljic1',
        password: 'suljopass'
    },
    {
        firstName: 'Avdo',
        lastName: 'Osmanovic',
        address: 'Viteška 1',
        birthdate: new Date(1965, 10, 11),
        umcn: 7023881122104,
        sex: 'M',
        phone: 38761555050,
        username: 'aosmanovic1',
        password: 'avdopass'
    },
    {
        firstName: 'Azra',
        lastName: 'Ledic',
        address: 'Sarajevska 4',
        birthdate: new Date(1780, 4, 13),
        umcn: 2784541888275,
        sex: 'F',
        phone: 38761986233,
        username: 'aledic1',
        password: 'azrapass'
    },
    {
        firstName: 'Muris',
        lastName: 'Medic',
        address: 'Bataljonska 1',
        birthdate: new Date(1963, 10, 1),
        umcn: 1223881122104,
        sex: 'M',
        phone: 38766555030,
        username: 'mmedic2',
        password: 'murispass'
    }
];

/*TEST CLASSBOOKS*/
let classBooks = [
    {
        grade: 1,
        class: 3,
    },
    {
        grade: 2,
        class: 4,
    },
    {
        grade: 4,
        class: 3,
    },
];

/*TEST STUDENTS*/
let students = [
    {
        firstName: 'Emir',
        lastName: 'Omeragic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'eomeragic1',
        password: 'emirpass',
    },
    {
        firstName: 'Haris',
        lastName: 'Palic',
        address: 'Sokolovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'hpalic1',
        password: 'harispass',
    },
    {
        firstName: 'Jasmin',
        lastName: 'Dudakovic',
        address: 'Ulica BB',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'jdudakovic1',
        password: 'jasminpass',
    },
    {
        firstName: 'Anesa',
        lastName: 'Zvizdic',
        address: 'Ferhat paše-sokolovića 2',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'F',
        phone: '38765222111',
        username: 'azvizdic1',
        password: 'anesapass',
    },
    {
        firstName: 'Mirnes',
        lastName: 'Muratspahic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'mmuratspahic3',
        password: 'mirnespass',
    },
    {
        firstName: 'Elvir',
        lastName: 'Omeragic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'eomeragic2',
        password: 'elvirpass',
    },
    {
        firstName: 'Vesna',
        lastName: 'Vizigot',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'F',
        phone: '38765222111',
        username: 'vvizigot1',
        password: 'vesnapass',
    },
    {
        firstName: 'Elvira',
        lastName: 'Omeragic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'F',
        phone: '38765222111',
        username: 'eomeragic3',
        password: 'elvirapass',
    },
    {
        firstName: 'Admir',
        lastName: 'Omerovic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'aomerovic2',
        password: 'admirpass',
    },
];

/*TEST PARENTS*/
let parents = [
    {
        firstName: 'Samir',
        lastName: 'Omeragic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1960, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'someragic1',
        password: 'samirpass',
    },
    {
        firstName: 'Dino',
        lastName: 'Palic',
        address: 'Sokolovicka 14',
        birthdate: new Date(1960, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'dpalic1',
        password: 'dinopass',
    },
    {
        firstName: 'Dina',
        lastName: 'Dudakovic',
        address: 'Ulica BB',
        birthdate: new Date(1960, 11, 15),
        umcn: 1043628783522,
        sex: 'F',
        phone: '38765222111',
        username: 'ddudakovic1',
        password: 'dinapass',
    },
    {
        firstName: 'Anes',
        lastName: 'Zvizdic',
        address: 'Ferhat paše-sokolovića 2',
        birthdate: new Date(1960, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'azvizdic2',
        password: 'anespass',
    },
    {
        firstName: 'Elvir',
        lastName: 'Muratspahic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1950, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'emuratspahic1',
        password: 'elvirpass',
    },
    {
        firstName: 'Elvira',
        lastName: 'Omeragic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1970, 11, 15),
        umcn: 1043628783522,
        sex: 'F',
        phone: '38765222111',
        username: 'eomeragic4',
        password: 'elvirpass',
    },
    {
        firstName: 'Vedran',
        lastName: 'Vizigot',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'vvizigot2',
        password: 'vedranpass',
    },
    {
        firstName: 'Vedad',
        lastName: 'Omeragic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1960, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'vomeragic1',
        password: 'vedadpass',
    },
    {
        firstName: 'Elvis',
        lastName: 'Omerovic',
        address: 'Zavidovicka 14',
        birthdate: new Date(1990, 11, 15),
        umcn: 1043628783522,
        sex: 'M',
        phone: '38765222111',
        username: 'eomerovic5',
        password: 'elvispass',
    },
];
/*TEST SUBJECTS*/
let subjects = [
    {
        name: 'Matematika',
        description: 'Lorem ipsum dolor sit amet, consectetur ' +
            'adipiscing elit. Vivamus efficitur convallis eros,' +
            ' viverra ullamcorper massa convallis at. Pellentesque ' +
            'nec sapien nec risus interdum euismod.',
        literature: 'Cras gravida, ipsum ut vulputate viverra, ' +
            'eros dui maximus elit, eu consequat nulla urna vitae arcu.',
        numberOfHours: 50,
    },
    {
        name: 'Fizika',
        description: 'Lorem ipsum dolor sit amet, consectetur ' +
            'adipiscing elit. Vivamus efficitur convallis eros,' +
            ' viverra ullamcorper massa convallis at. Pellentesque ' +
            'nec sapien nec risus interdum euismod.',
        literature: 'Cras gravida, ipsum ut vulputate viverra, ' +
            'eros dui maximus elit, eu consequat nulla urna vitae arcu.',
        numberOfHours: 40,
    },
    {
        name: 'Geografija',
        description: 'Lorem ipsum dolor sit amet, consectetur ' +
            'adipiscing elit. Vivamus efficitur convallis eros,' +
            ' viverra ullamcorper massa convallis at. Pellentesque ' +
            'nec sapien nec risus interdum euismod.',
        literature: 'Cras gravida, ipsum ut vulputate viverra, ' +
            'eros dui maximus elit, eu consequat nulla urna vitae arcu.',
        numberOfHours: 30,
    },
    {
        name: 'Hemija',
        description: 'Lorem ipsum dolor sit amet, consectetur ' +
            'adipiscing elit. Vivamus efficitur convallis eros,' +
            ' viverra ullamcorper massa convallis at. Pellentesque ' +
            'nec sapien nec risus interdum euismod.',
        literature: 'Cras gravida, ipsum ut vulputate viverra, ' +
            'eros dui maximus elit, eu consequat nulla urna vitae arcu.',
        numberOfHours: 25,
    },
    {
        name: 'Historija',
        description: 'Lorem ipsum dolor sit amet, consectetur ' +
            'adipiscing elit. Vivamus efficitur convallis eros,' +
            ' viverra ullamcorper massa convallis at. Pellentesque ' +
            'nec sapien nec risus interdum euismod.',
        literature: 'Cras gravida, ipsum ut vulputate viverra, ' +
            'eros dui maximus elit, eu consequat nulla urna vitae arcu.',
        numberOfHours: 30,
    },
];

const deleteAll = () => {
    return new Promise((resolve, reject) => {
        Teacher.remove({}).exec()
            .then(ClassBook.remove({}).exec()
                .then(Student.remove({}).exec()
                    .then(Parent.remove({}).exec()
                        .then(Subject.remove({}).exec()
                        )
                    )
                )
            )
    })
};

const populateApp = () => {
    teachers.forEach(teacher => {
        teacher.password = bcrypt.hashSync(teacher.password, saltRounds);
    });
    students.forEach(student => {
        student.password = bcrypt.hashSync(student.password, saltRounds);
    });
    parents.forEach(parent => {
        parent.password = bcrypt.hashSync(parent.password, saltRounds);
    });
    admin.password = bcrypt.hashSync(admin.password, saltRounds);
    deleteAll().then(
        Teacher.create(teachers, (err) => {
            if (err) {
                reject(err);
            }
            Teacher.find({}, (err, teachers) => {
                if (err) {
                    reject(err);
                }
                classBooks[0].teacherId = teachers[0]._id;
                classBooks[1].teacherId = teachers[1]._id;
                classBooks[2].teacherId = teachers[4]._id;
                ClassBook.create(classBooks, err => {
                    if (err) reject(err);
                    ClassBook.find({}, (err, data) => {
                        if (err) reject(err);
                        students[0].classBookId = data[0]._id;
                        students[1].classBookId = data[0]._id;
                        students[2].classBookId = data[0]._id;

                        students[3].classBookId = data[0]._id;
                        students[4].classBookId = data[0]._id;
                        students[5].classBookId = data[1]._id;

                        students[6].classBookId = data[0]._id;
                        students[7].classBookId = data[2]._id;
                        students[8].classBookId = data[2]._id;

                        Parent.create(parents, (err, parentData) => {
                            if (err) reject(err);
                            students.forEach((s, index) => {
                                s.parentId = parentData[index]._id;

                            });

                            Student.create(students, (err) => {
                                if (err) reject(err);

                                subjects.forEach((subject, index) => subject.teacherId = teachers[index]._id);
                                Subject.create(subjects)
                                    .then(() => {
                                        console.log('Populated...')
                                    })
                            });

                        })
                    });
                })
            });
        })
    );


};

module.exports = populateApp;