const router = require('express').Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Teacher = new require('../models/Teacher');
const Student = new require('../models/Student');
const Parent = new require('../models/Parent');

const adminConstants = require('../config/admin.config');

router.post('/login', (req, res) => {

    const receivedPassword = req.body.password;
    const receivedUsername = req.body.username;

    let user = {};

    Teacher.findOne({
        username: receivedUsername,
    }, (err, data) => {
        if (err) {
            console.error(err);
            res.status(500).end('Error while finding teacher');
        }
        if (data) {
            user.role = 'teacher';
            user.id = data._id;
            user.password = data.password;
        }
    }).then(() => {
        if (!user.hasOwnProperty('role')) {
            Student.findOne({
                username: receivedUsername,
            }, (err, data) => {
                if (err) {
                    console.error(err);
                    res.status(500).end('Error while finding student');
                }
                if (data) {
                    user.role = 'student';
                    user.id = data._id;
                    user.password = data.password;
                }
            }).then(() => {
                if (!user.hasOwnProperty('role')) {
                    Parent.findOne({
                        username: receivedUsername,
                    }, (err, data) => {
                        if (err) {
                            console.error(err);
                            res.status(500).end('Error while finding parent');
                        }
                        if (data) {
                            user.role = 'parent';
                            user.id = data._id;
                            user.password = data.password;
                        }
                    }).then(() => {
                        if (receivedUsername === adminConstants.username) {
                            user.role = 'admin';
                            user.id = null;
                            user.password = adminConstants.password;
                        }
                        loginSuccess(user);
                    }).catch(err => console.error(err));
                } else {
                    loginSuccess(user);
                }
            }).catch(err => console.error(err));
        } else {
            loginSuccess(user);
        }
    }).catch(err => console.error(err));

    const loginSuccess = (user) => {
        if (Object.keys(user).length === 0 || !bcrypt.compareSync(receivedPassword, user.password)) {
            res.status(200).end(JSON.stringify({user: {}}));
        } else {
            const token = jwt.sign({
                sub: user.id,
                username: receivedUsername,
                role: user.role,
            }, 'mysecretkey', {expiresIn: '8 hours'});
            res.status(200).end(JSON.stringify({
                acces_token: token,
                user: {role: user.role, id: user.id, username: user.username}
            }));
        }
    }
});

module.exports = router;