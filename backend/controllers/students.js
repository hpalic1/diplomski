const router = require('express').Router();

const Student = new require('../models/Student');
const bcrypt = require('bcrypt');
const saltRounds = 5;

router.get('/student', (req, res) => {
    Student.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/student/id/:id', (req, res) => {
    let id = req.params.id;

    Student.findOne({_id: id}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/student/:lastName', (req, res) => {
    let lastName = req.params.lastName;

    Student.find({lastName: lastName}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/student/classBook/:id', (req, res) => {
    let id = req.params.id;

    Student.find({classBookId: id}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.post('/student', (req, res) => {
    let values = req.body;
    bcrypt.hash(values.password, saltRounds).then(function (hash) {
        values.password = hash;
        Student.create(values, (err, data) => {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            else {
                res.status(201).json({
                    message: 'Student successfully created.',
                    data: data
                });
            }

        });
    });


});

router.put('/student', (req, res) => {
    let body = req.body;
    Student.findById(body.id, function (err, student) {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }

        student.firstName = body.firstName;
        student.lastName = body.lastName;
        student.birthdate = body.birthdate;
        student.address = body.address;
        student.umcn = body.umcn;
        student.sex = body.sex;
        student.phone = body.phone;

        bcrypt.hash(body.password, saltRounds).then(function (hash) {
            if (body.changePassword) {
                student.password = hash;
            }
            student.save(function (err, updatedStudent) {
                if (err) {
                    for (let errorField in err.errors) {
                        console.log(err.errors[errorField].message);
                    }
                    res.status(500).end(JSON.stringify(err.errors));
                }
                res.send(updatedStudent);
            });
        });
    });
});

router.put('/student/mark', (req, res) => {
    let body = req.body;
    Student.findById(body.id, function (err, student) {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }


        if (body.editFinalGrade) {
            student.marks.forEach(mark => {
                if (mark.finalGrade && mark.semester === body.mark.semester) {
                    mark.value = body.mark.value;
                    mark.date = body.mark.date;
                }
            })
        } else {
            student.marks.push(body.mark);
        }

        student.save(function (err, updatedStudent) {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            res.send(updatedStudent);
        });
    });
});

router.put('/student/behavior', (req, res) => {
    let body = req.body;
    Student.findById(body.id, function (err, student) {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }

        student.behavior = body.behavior;

        student.save(function (err, updatedStudent) {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            res.send(updatedStudent);
        });
    });
});

module.exports = router;