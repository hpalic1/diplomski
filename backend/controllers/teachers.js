const router = require('express').Router();

const Teacher = new require('../models/Teacher');
const validateTeacher = require('./validation');

const bcrypt = require('bcrypt');
const saltRounds = 5;

router.get('/teacher', (req, res) => {
    Teacher.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/teacher/:lastName', (req, res) => {
    let lastName = req.params.lastName;

    Teacher.find({lastName: lastName}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/teacher/id/:id', (req, res) => {
    let id = req.params.id;

    Teacher.findOne({_id: id}, (err, data) => {
        if (err) {
            console.error(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

// router.post('/teacher', validateTeacher, (req, res) => {
router.post('/teacher', (req, res) => {
    let values = req.body;
    bcrypt.hash(values.password, saltRounds).then(function (hash) {
        values.password = hash;
        Teacher.create(values, (err, data) => {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            else {
                res.status(201).json({
                    message: 'Teacher successfully created.',
                    data: data
                });
            }

        });
    });

});

router.put('/teacher', (req, res) => {
    let body = req.body;
    Teacher.findById(body.id, function (err, teacher) {
        console.log(teacher);
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }

        teacher.firstName = body.firstName;
        teacher.lastName = body.lastName;
        teacher.birthdate = body.birthdate;
        teacher.address = body.address;
        teacher.umcn = body.umcn;
        teacher.sex = body.sex;
        teacher.phone = body.phone;

        bcrypt.hash(body.password, saltRounds).then(function (hash) {
            if (body.changePassword) {
                student.password = hash;
            }
            teacher.save(function (err, updatedTeacher) {
                if (err) {
                    for (let errorField in err.errors) {
                        console.log(err.errors[errorField].message);
                    }
                    res.status(500).end(JSON.stringify(err.errors));
                }
                res.send(updatedTeacher);
            });
        });
    });
});

module.exports = router;