const router = require('express').Router();

const Parent = new require('../models/Parent');
const bcrypt = require('bcrypt');
const saltRounds = 5;

router.get('/parent', (req, res) => {
    Parent.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    })
});

router.get('/parent/id/:id', (req, res) => {
    let id = req.params.id;

    Parent.findOne({_id: id}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/parent/:lastName', (req, res) => {
    let lastName = req.params.lastName;

    Parent.find({lastName: lastName}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.post('/parent', (req, res) => {
    let values = req.body;
    bcrypt.hash(values.password, saltRounds).then(function (hash) {
        values.password = hash;
        Parent.create(values, (err, data) => {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            else {
                res.status(201).json({
                    message: 'Parent successfully created.',
                    data,
                })
            }
        })
    });
});

router.put('/parent', (req, res) => {
    let body = req.body;
    Parent.findById(body.id, function (err, parent) {
        console.log(parent);
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }

        parent.firstName = body.firstName;
        parent.lastName = body.lastName;
        parent.birthdate = body.birthdate;
        parent.address = body.address;
        parent.umcn = body.umcn;
        parent.sex = body.sex;
        parent.phone = body.phone;

        bcrypt.hash(body.password, saltRounds).then(function (hash) {
            if (body.changePassword) {
                student.password = hash;
            }
            parent.save(function (err, updatedParent) {
                if (err) {
                    for (let errorField in err.errors) {
                        console.log(err.errors[errorField].message);
                    }
                    res.status(500).end(JSON.stringify(err.errors));
                }
                res.send(updatedParent);
            });
        });
    });
});

module.exports = router;