const validator = require('validator');

const validateTeacher = (req, res, next) => {
    console.log(req.body);
    let {
        firstName, lastName, address, birthdate, umcn, sex, phone, username, passwordHash
    } = req.body;

    console.log(!(sex === 'M' || sex === 'F'));
    console.log(sex === null);

    if (!firstName || !validator.isAlpha(firstName, 'sr-RS@latin')) {
        res.status(400).json({
            error: 'First name must consist of letters only',
        });
    }
    else if (!lastName || !validator.isAlpha(lastName, 'sr-RS@latin')) {
        res.status(400).json({
            error: 'Last name must consist of letters only'
        });
    }
    else if (!umcn || !validator.isLength(umcn, {min: 13, max: 13})) {
        res.status(400).json({
            error: 'Unique master citizen number must have 13 digits'
        });
    }
    else if (!username) {
        res.status(400).json({
            error: 'Username must exist'
        });
    }
    else if (!passwordHash) {
        res.status(400).json({
            error: 'Password must exist'
        });
    }
    else if (!(sex === 'M' || sex === 'F')) {
        res.status(400).json({
            error: 'Sex must be male M or female F'
        })
    }
    else {
        next();
    }
};

module.exports = validateTeacher;