const router = require('express').Router();

const ClassBook = new require('../models/ClassBook');

router.get('/classBook', (req, res) => {
    ClassBook.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/classBook/id/:id', (req, res) => {
    let id = req.params.id;

    ClassBook.findOne({_id: id}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.put('/classBook', (req, res) => {
    let body = req.body;
    ClassBook.findById(body.id, function (err, classBook) {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }

        console.log(classBook);
        classBook.workingWeeks.push(body.week);

        classBook.save(function (err, updatedClassBook) {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            res.send(updatedClassBook);
        });
    });
});

router.put('/classBook/workingHour', (req, res) => {
    let body = req.body;
    ClassBook.findById(body.id, function (err, classBook) {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }

        classBook.workingWeeks.forEach(week => {
            week.workingDays.forEach(day => {
                day.workingHours.forEach(hour => {
                    if (hour._id.toString() === body.workingHourId) {
                        hour.subjectId = body.workingHour.subjectId;
                        hour.schoolHourNumber = body.workingHour.schoolHourNumber;
                        hour.note = body.workingHour.note;
                        hour.content = body.workingHour.content;
                        hour.workingHourNumber = body.workingHour.workingHourNumber;
                    }
                });

            });
        });

        classBook.save(function (err, updatedClassBook) {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            res.send(updatedClassBook);
        });
    });
});

router.post('/classBook', (req, res) => {
    let values = req.body;
    ClassBook.create(values, (err, data) => {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }
        else {
            res.status(201).json({
                message: 'ClassBook successfully created.',
                data: data
            });
        }

    });
});

module.exports = router;