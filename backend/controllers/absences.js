const router = require('express').Router();

const Absence = new require('../models/Absence');

router.get('/absence', (req, res) => {
    Absence.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.post('/absence', (req, res) => {
    let values = req.body;

    Absence.create(values, (err, data) => {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }
        else {
            res.status(201).json({
                message: 'Absence successfully created.',
                data: data
            });
        }

    });
});

module.exports = router;