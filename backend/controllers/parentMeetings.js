const router = require('express').Router();

const ParentMeeting = new require('../models/ParentMeeting');

router.get('/parentMeeting', (req, res) => {
    ParentMeeting.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    })
});

router.get('/parentMeeting/:id', (req, res) => {
    let id = req.params.id;

    ParentMeeting.find({teacherId: id}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    })
});

router.post('/parentMeeting', (req, res) => {
    let values = req.body;
    console.log(values);

    ParentMeeting.create(values, (err, data) => {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }
        else {
            res.status(201).json({
                message: 'ParentMeeting successfully created.',
                data,
            })
        }
    })
});

module.exports = router;