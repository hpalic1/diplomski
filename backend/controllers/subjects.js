const router = require('express').Router();

const Subject = new require('../models/Subject');

router.get('/subject', (req, res) => {
    Subject.find({}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.get('/subject/id/:id', (req, res) => {
    let id = req.params.id;

    Subject.findOne({_id: id}, (err, data) => {
        if (err) {
            console.log(err);
            res.status(500).end();
        }
        res.status(200).send(data);
    });
});

router.post('/subject', (req, res) => {
    let values = req.body;

    Subject.create(values, (err, data) => {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }
        else {
            res.status(201).json({
                message: 'Subject successfully created.',
                data: data
            });
        }

    });
});

router.put('/subject', (req, res) => {
    let body = req.body;
    Subject.findById(body.subjectId, function (err, subject) {
        if (err) {
            for (let errorField in err.errors) {
                console.log(err.errors[errorField].message);
            }
            res.status(500).end(JSON.stringify(err.errors));
        }

        subject.teacherId = body.teacherId;

        subject.save(function (err, updatedSubject) {
            if (err) {
                for (let errorField in err.errors) {
                    console.log(err.errors[errorField].message);
                }
                res.status(500).end(JSON.stringify(err.errors));
            }
            res.send(updatedSubject);
        });
    });
});

module.exports = router;