const populateApp = require("./test_data/populateApp");

const bodyParser = require('body-parser');
const expressJwt = require('express-jwt');

const express = require('express');
const cors = require('cors');

const teacherRoutes = require('./controllers/teachers');
const parentRoutes = require('./controllers/parents');
const studentRoutes = require('./controllers/students');
const classBookRoutes = require('./controllers/classBooks');
const loginRoutes = require('./controllers/login');
const subjectRoutes = require('./controllers/subjects');
const parentMeetingRoutes = require('./controllers/parentMeetings');

const jwtCheck = expressJwt({
    secret: 'mysecretkey',
});

const unless = function (path, middleware) {
    return function (req, res, next) {
        if (path === req.path) {
            return next();
        } else {
            return middleware(req, res, next);
        }
    };
};

const app = express();


app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(unless('/login', jwtCheck));

app.use(teacherRoutes);
app.use(classBookRoutes);
app.use(loginRoutes);
app.use(studentRoutes);
app.use(parentRoutes);
app.use(subjectRoutes);
app.use(parentMeetingRoutes);

app.get('/', function (req, res) {
    res.send('It works');
});

app.listen(process.env.PORT || 8080, () => {
    console.log('Server started...');

    populateApp();
});
