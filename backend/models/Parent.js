const {mongoose} = require('../config/config');

const ObjectId = mongoose.Schema.Types.ObjectId;

const ParentSchema = mongoose.Schema({
    firstName: {type: String},
    lastName: {type: String},
    address: {type: String},
    birthdate: {type: Date},
    umcn: {type: String},
    sex: {type: String},
    phone: {type: String},
    username: {type: String},
    password: {type: String},
    picture: {type: Buffer},
    studentId: {type: ObjectId},
    createdAt: {type: Date, default: Date.now},
    modifiedAd: {type: Date, default: Date.now},
});

module.exports = mongoose.model('Parent', ParentSchema);