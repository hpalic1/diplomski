const {mongoose} = require('../config/config');

const ObjectId = mongoose.Schema.Types.ObjectId;

const AbsenceSchema = mongoose.Schema({
    studentId: {type: ObjectId},
    hourId: {type: ObjectId},
    value: {type: String},
    reason: {type: String},
    createdAt: {type: Date, default: Date.now},
    modifiedAd: {type: Date, default: Date.now},
});

module.exports = mongoose.model('Absence', AbsenceSchema);