const {mongoose} = require('../config/config');

const ObjectId = mongoose.Schema.Types.ObjectId;

const SubjectScheme = mongoose.Schema({
    name: {type: String},
    description: {type: String},
    literature: {type: String},
    numberOfHours: {type: Number},
    teacherId: {type: ObjectId},
    createdAt: {type: Date, default: Date.now},
    modifiedAd: {type: Date, default: Date.now},
});

module.exports = mongoose.model('Subject', SubjectScheme);