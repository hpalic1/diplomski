const {mongoose} = require('../config/config');

const ObjectId = mongoose.Schema.Types.ObjectId;

const ParentMeetingSchema = mongoose.Schema({
    date: {type: Date},
    content: {type: String},
    teacherId: {type: ObjectId},
    createdAt: {type: Date, default: Date.now},
    modifiedAd: {type: Date, default: Date.now},
});

module.exports = mongoose.model('ParentMeeting', ParentMeetingSchema);