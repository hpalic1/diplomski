const {mongoose} = require('../config/config');

const ObjectId = mongoose.Schema.Types.ObjectId;

const StudentSchema = mongoose.Schema({
    firstName: {type: String},
    lastName: {type: String},
    address: {type: String},
    birthdate: {type: Date},
    umcn: {type: String},
    sex: {type: String},
    phone: {type: String},
    username: {type: String},
    password: {type: String},
    picture: {type: Buffer},
    subjectList: [{
        subjectId: {type: ObjectId}
    }],
    behavior: [{
        semester: {type: Number},
        value: {type: String},
    }],
    marks: [{
        subjectId: {type: ObjectId},
        value: {type: Number},
        date: {type: Date},
        note: {type: String},
        semester: {type: Number},
        finalGrade: {type: Boolean}
    }],
    classBookId: {type: ObjectId},
    parentId: {type: ObjectId},
    createdAt: {type: Date, default: Date.now},
    modifiedAd: {type: Date, default: Date.now},
});

module.exports = mongoose.model('Student', StudentSchema);