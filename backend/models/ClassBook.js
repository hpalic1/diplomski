const {mongoose} = require('../config/config');

const ObjectId = mongoose.Schema.Types.ObjectId;

const ClassBookSchema = mongoose.Schema({
    teacherId: {type: ObjectId},
    grade: {type: Number},
    class: {type: Number},
    workingWeeks: [{
        workingDays: [{
            workingHours: [{
                subjectId: {type: ObjectId},
                schoolHourNumber: {type: Number},
                note: {type: String},
                content: {type: String},
                workingHourNumber: {type: Number},
            }],
            workingDayNumber: {type: Number},
            date: {type: Date}
        }],
        workingWeekNumber: {type: Number},
    }],
    parentMeetings: [{
        record: {type: String},
    }],
    createdAt: {type: Date, default: Date.now},
    modifiedAd: {type: Date, default: Date.now},
});

module.exports = mongoose.model('ClassBook', ClassBookSchema);
