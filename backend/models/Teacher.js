const {mongoose} = require('../config/config');

const TeacherSchema = mongoose.Schema({
    firstName: {type: String, required: true},
    lastName: {type: String, required: true},
    address: {type: String, default: null},
    birthdate: {type: Date, default: null},
    umcn: {type: String, required: true},
    sex: {type: String, required: true},
    phone: {type: String, default: null},
    username: {type: String, required: true},
    password: {type: String, required: true},
    createdAt: {type: Date, default: Date.now},
    modifiedAd: {type: Date, default: Date.now},
});

module.exports = mongoose.model('Teacher', TeacherSchema);