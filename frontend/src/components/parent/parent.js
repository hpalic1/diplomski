import React from 'react';
import StudentData from "../student/studentData";
import helpers from "../../helpers/storage.helper";
import {getAllUsers} from "../admin/actions";

export default class Parent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            studentId: null,
        }
    }

    componentDidMount() {
        getAllUsers('student')
            .then(students => {
                students.forEach(student => {
                    if (student.parentId === helpers.getCurrentUser().id) {
                        this.setState({
                            studentId: student._id,
                        });
                    }
                })
            })
            .catch(err => console.error(err));
    }

    render() {
        return (
            this.state.studentId ?
                <StudentData studentId={this.state.studentId}/>
                : null
        );
    }

};