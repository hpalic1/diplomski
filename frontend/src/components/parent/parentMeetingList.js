import React from 'react';
import uuid from 'uuid';
import {getParentMeetings} from "./actions";
import helpers from "../../helpers/storage.helper";
import {getAllUsers} from "../admin/actions";

export default class ParentMeetingList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            parentMeetings: [],
            content: '',
        }
    };

    refreshParentMeetings = () => {
        getAllUsers('student')
            .then(students => {
                students.forEach(s => {
                    if (s.parentId === helpers.getCurrentUser().id) {
                        getAllUsers('classBook')
                            .then(classBooks => {
                                classBooks.forEach(c => {
                                    if (c._id === s.classBookId) {
                                        getParentMeetings(c.teacherId)
                                            .then((parentMeetings) => {
                                                this.setState({
                                                    parentMeetings,
                                                })
                                            })
                                            .catch(err => console.error(err));
                                    }
                                })
                            })
                            .catch(err => console.error(err));
                    }
                })
            })
            .catch(err => console.error(err));
    };

    componentDidMount() {
        this.refreshParentMeetings();
    }

    onParentMeetingClick = (index) => {
        this.setState({
            content: this.state.parentMeetings[index].content,
        })
    };

    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-12 col-md-12 col-sm-12'>
                        <div className='row text-align-center'>
                            <h4>Pregled zapisnika sa roditeljskog sastanka</h4>
                        </div>
                        <br/>
                        <div className='row'>
                            <div className='col-lg-6 col-md-6 col-sm-6'>
                                <h4>Lista roditeljskih sastanaka</h4>
                                <table className='table'>
                                    <tbody>
                                    {
                                        this.state.parentMeetings.map((p, index) => {
                                            return (
                                                <tr key={uuid()} className='cursor-pointer'>
                                                    <td onClick={() => this.onParentMeetingClick(index)}>
                                                        {new Date(p.date).toLocaleDateString('sr')}
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }
                                    </tbody>
                                </table>
                            </div>
                            <div className='col-lg-6 col-md-6 col-sm-6'>
                                <textarea
                                    className='form-control resize-none read-only-background'
                                    rows={20}
                                    name='content'
                                    value={this.state.content}
                                    readOnly={true}
                                >
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }

}