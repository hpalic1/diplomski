import {baseURL} from "../../config";
import helpers from "../../helpers/storage.helper";

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    let error = new Error(`Http Error ${response.statusText}`);
    error.status = response.status;
    error.respones = response;
    console.log(error);
    throw error;
}

export const getParentMeetings = (teacherId) => {
    let message = {
        successMessage: '',
        errorMessage: '',
    };

    return new Promise((resolve, reject) => {
        fetch(`${baseURL}/parentMeeting/${teacherId}`, {
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                message.errorMessage = 'Desila se greška';
                reject(message);
            })
    });
};