import React from 'react';


class CloseAlert extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            style: {
                float: 'right',
                textDecoration: 'none',
                color: 'black',
                opacity: 0.2,
            }
        };
    }

    setStyleOnEnter = (e) => {
        let style = Object.assign({}, this.state.style);

        style.opacity = 1;

        this.setState({
            style,
        })
    };

    setStyleOnLeave = (e) => {

        let style = Object.assign({}, this.state.style);

        style.opacity = 0.2;
        this.setState({
            style,
        })
    };

    handleClick = (e) => {
        this.props.onClick(e);
    };

    render() {
        return (
            <a
                role="button"
                onClick={this.handleClick}
                style={this.state.style}
                onMouseEnter={this.setStyleOnEnter}
                onMouseLeave={this.setStyleOnLeave}
            >
                X</a>
        );
    }
}

export default class Alert extends React.Component {

    handleClick = (e) => {
        this.props.onClick(e);
    };

    className = () => {
        return 'alert alert-dismissible ' + this.props.type;
    };

    render() {
        return (
            <div className={this.className()}>
                <strong>{this.props.strong}</strong> {this.props.message}
                <CloseAlert
                    onClick={this.handleClick}
                />
            </div>
        );
    }
}

