import React from 'react';
import {Link} from 'react-router-dom';
import '../../css/style.css';
import '../../css/behaviorColor.css';

const uuidv4 = require('uuid');

class Navbar extends React.Component {

    render() {
        let logoutButton = <li key={uuidv4()}>
            <Link className='cursor-pointer padding-right' to='/logout'>
                Log out
            </Link>
        </li>;

        let toRender = this.props.links.map((link) => {
            return (
                <li key={uuidv4()}>
                    <Link
                        to={link.route}>
                        {link.name}
                    </Link>
                </li>
            );
        });

        return (
            <nav className="navbar navbar-default navbar-inverse">
                <ul className="nav navbar-nav">
                    {toRender}
                </ul>
                <ul className='nav navbar-nav navbar-right-logout'>
                    {logoutButton}
                </ul>
            </nav>
        );
    }
}

export default Navbar;