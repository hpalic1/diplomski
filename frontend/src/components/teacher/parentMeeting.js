import React from 'react';
import uuid from 'uuid';
import {addParentMeeting} from "./actions";
import helpers from "../../helpers/storage.helper";
import Alert from "../alert/alert";
import {getParentMeetings} from "../parent/actions";

export default class ParentMeeting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            parentMeetings: [],
            content: '',
            newParentMeeting: {
                content: '',
                date: null,
            },
            messages: {
                errorMessage: '',
                successMessage: '',
            }
        }
    };

    refreshParentMeetings = () => {
        getParentMeetings(helpers.getCurrentUser().id)
            .then(parentMeetings => {
                this.setState({
                    parentMeetings,
                })
            })
            .catch(err => console.error(err));
    };

    componentDidMount() {
        this.refreshParentMeetings();
    }

    onParentMeetingClick = (index) => {
        this.setState({
            content: this.state.parentMeetings[index].content,
        })
    };

    addParentMeeting = () => {
        let data = {
            content: this.state.newParentMeeting.content,
            date: this.state.newParentMeeting.date,
            teacherId: helpers.getCurrentUser().id,
        };
        addParentMeeting(data)
            .then(res => {
                this.setState({
                    messages: {
                        successMessage: 'Uspješno zapisan roditeljski sastanak',
                    }
                });
                this.refreshParentMeetings();
            })
            .catch(err => console.error(err));
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                        <div className='row text-align-center'>
                            <h4>Pregled zapisnika sa roditeljskog sastanka</h4>
                        </div>
                        <br/>
                        <div className='row'>
                            <div className='col-lg-6 col-md-6 col-sm-6'>
                                <h4>Lista roditeljskih sastanaka</h4>
                                <table className='table'>
                                    <tbody>
                                    {
                                        this.state.parentMeetings.map((p, index) => {
                                            return (
                                                <tr key={uuid()} className='cursor-pointer'>
                                                    <td onClick={() => this.onParentMeetingClick(index)}>
                                                        {new Date(p.date).toLocaleDateString('sr')}
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }
                                    </tbody>
                                </table>
                            </div>
                            <div className='col-lg-6 col-md-6 col-sm-6'>
                                <textarea
                                    className='form-control resize-none read-only-background'
                                    rows={20}
                                    name='content'
                                    value={this.state.content}
                                    readOnly={true}
                                >
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                        <div className='row text-align-center'>
                            <h4>Unesite zapisnik sa sastanka</h4>
                        </div>
                        <br/>
                        <div className='row'>
                            <div className='col-lg-12 col-md-12 col-sm-12'>
                                <textarea
                                    className='form-control resize-none'
                                    rows={20}
                                    name='content'
                                    value={this.state.newParentMeeting.content}
                                    onChange={(e) => this.setState({
                                        newParentMeeting: {
                                            content: e.target.value,
                                            date: JSON.parse(JSON.stringify(new Date()))
                                        }
                                    })}
                                >
                                </textarea>
                            </div>
                        </div>
                        <br/>
                        <div className='col-lg-10 col-lg-offset-1 form-group'>
                            {
                                this.state.messages.errorMessage &&
                                <Alert
                                    onClick={this.clearFormMessage}
                                    type='alert-danger'
                                    strong='Problem!'
                                    message={this.state.messages.errorMessage}
                                />
                            }
                            {
                                this.state.messages.successMessage &&
                                <Alert
                                    onClick={this.clearFormMessage}
                                    type='alert-success'
                                    strong='Uspjeh!'
                                    message={this.state.messages.successMessage}
                                />
                            }
                        </div>
                        <div className='row text-align-center'>
                            <button
                                className="btn btn-primary text-center"
                                type="button"
                                onClick={(e) => this.addParentMeeting(e)}
                            >
                                Spremite zapisnik
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        );
    }

}