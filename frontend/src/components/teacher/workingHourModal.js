import React from 'react';
import uuid from "uuid";
import NumericInput from "react-numeric-input";
import constants from "../../config/classConstants.config";
import helpers from "../../helpers/storage.helper";
import {Modal} from "react-bootstrap";
import Alert from "../alert/alert";
import {addWorkingHour} from "./actions";

export default class WorkingHourModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            workingDay: helpers.getCurrentWorkingDay(),
            selectedSchoolHourNumber: null,
            workingHourNumber: null,
            selectedSubjectId: null,
            selectedSubject: '',
            note: '',
            content: '',
            messages: {
                successMessage: '',
                errorMessage: '',
            }
        };
    }

    componentWillReceiveProps(props) {
        this.setState({
            selectedSchoolHourNumber: 1,
            selectedSubjectId: props.subjects.length > 0 ? props.subjects[0]._id : null,
            selectedSubject: props.subjects.length > 0 ? props.subjects[0].name : '',
        })
    }

    onSelectSchoolHourNumberChange = (e) => {
        this.setState({
            selectedSchoolHourNumber: e.nativeEvent.target.selectedIndex + 1,
        })
    };

    handleWorkingHourNumberChange = (value) => {
        this.setState({
            workingHourNumber: value,
        })
    };

    onSelectSubject = (e) => {
        this.setState({
            selectedSubjectId: this.props.subjects[e.nativeEvent.target.selectedIndex]._id,
            selectedSubject: this.props.subjects[e.nativeEvent.target.selectedIndex].name,
        })
    };

    onNoteChange = (e) => {
        this.setState({
            note: e.target.value,
        })
    };

    onContentChange = (e) => {
        this.setState({
            content: e.target.value,
        })
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    validateWorkingHourModal = () => {
        if (!this.state.workingHourNumber) {
            return 'Unesite broj nastavnog časa';
        }
        if (!this.state.selectedSubjectId) {
            return 'Odaberite predmet';
        }
        if (!this.state.content) {
            return 'Sadržaj ne može biti prazan';
        }
        return null;
    };

    onAddHour = () => {
        let errorMessage = this.validateWorkingHourModal();
        if (errorMessage) {
            this.setState({
                messages: {
                    errorMessage: errorMessage,
                }
            });
            return;
        }

        let workingDay = JSON.parse(JSON.stringify(this.state.workingDay));
        let hours = workingDay.workingHours;
        let index = this.state.selectedSchoolHourNumber - 1;
        hours[index].subjectId = this.state.selectedSubjectId;
        hours[index].schoolHourNumber = this.state.selectedSchoolHourNumber;
        hours[index].note = this.state.note;
        hours[index].content = this.state.content;
        hours[index].workingHourNumber = this.state.workingHourNumber;

        let classBook = helpers.getCurrentClassBook();

        let workingHourId = null;

        classBook.workingWeeks.forEach(week => {
            week.workingDays.forEach(day => {
                if (workingDay._id === day._id) {
                    workingHourId = day.workingHours[index]._id;
                }
            });
        });

        let data = {
            id: helpers.getCurrentClassBook()._id,
            workingHour: workingDay.workingHours[index],
            workingHourId: workingHourId,

        };

        addWorkingHour(data)
            .then(result => {
                helpers.setCurrentWorkingDay(workingDay);
                this.setState({
                    workingDay: workingDay,
                    selectedSchoolHourNumber: null,
                    workingHourNumber: null,
                    selectedSubjectId: null,
                    selectedSubject: '',
                    note: '',
                    content: '',
                    messages: {
                        successMessage: '',
                        errorMessage: '',
                    }
                }, () => this.props.closeModal());

            })
            .catch(err => console.log(err));


    };

    render() {
        return (
            <Modal show={this.props.workingHourModal.show} className='working-hour-modal'>
                <div className='container container-wide'>
                    <div className='row'>
                        <div className='col-lg-4 float-left'>
                            <h4>
                                {this.state.workingDay.workingDayNumber + '. radni dan'}
                            </h4>
                        </div>
                        <div className='col-lg-4'>
                            <h4>
                                {'Upisavanje časa'}
                            </h4>
                        </div>
                        <div className='col-lg-4'>
                            <h4 className='float-right'>
                                {new Date(this.state.workingDay.date).toLocaleDateString('sr')}
                            </h4>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-lg-6'>
                            <div className='row'>
                                <div className='col-lg-12'>
                                    <label>Nastavni čas</label>
                                    <select className='form-control'
                                            onChange={(e) => this.onSelectSchoolHourNumberChange(e)}
                                            value={this.state.selectedSchoolHourNumber + '.'}
                                    >
                                        {
                                            this.state.workingDay.workingHours.map((workingHour) => {
                                                return (
                                                    <option key={uuid()}>
                                                        {workingHour.schoolHourNumber + '.'}
                                                    </option>
                                                );
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            <br/>
                            <div className='row'>
                                <div className='col-lg-12'>
                                    <label>Broj nastavnog časa</label>
                                    <NumericInput
                                        className='form-control'
                                        name='workingHourNumber'
                                        min={1}
                                        max={constants.maxWorkingHourNumber}
                                        value={this.state.workingHourNumber}
                                        strict
                                        onChange={(e) => this.handleWorkingHourNumberChange(e)}
                                    />
                                </div>
                            </div>
                            <br/>
                            <div className='row'>
                                <div className='col-lg-12'>
                                    <label>Odaberi predmet</label>
                                    <select className='form-control'
                                            onChange={(e) => this.onSelectSubject(e)}
                                            value={this.state.selectedSubject}
                                    >
                                        {
                                            this.props.subjects.map((subject) => {
                                                return (
                                                    <option key={uuid()}>
                                                        {subject.name}
                                                    </option>
                                                );
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            <br/>
                            <div className='row'>
                                <div className='col-lg-12'>
                                    <label>Napomena</label>
                                    <textarea
                                        className='form-control resize-none'
                                        rows={5}
                                        name='note'
                                        value={this.state.note}
                                        onChange={(e) => this.onNoteChange(e)}
                                    >
                                    </textarea>
                                </div>
                            </div>
                            <br/>
                        </div>
                        <div className='col-lg-6'>
                            <div className='row'>
                                <div className='col-lg-12'>
                                    <label>Sadržaj</label>
                                    <textarea
                                        className='form-control resize-none'
                                        rows={17}
                                        name='content'
                                        value={this.state.content}
                                        onChange={(e) => this.onContentChange(e)}
                                    >
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-lg-12'>
                            <div className='row'>
                                <div className='col-lg-10 col-lg-offset-1 form-group'>
                                    {
                                        this.state.messages.errorMessage &&
                                        <Alert
                                            onClick={this.clearFormMessage}
                                            type='alert-danger'
                                            strong='Problem!'
                                            message={this.state.messages.errorMessage}
                                        />
                                    }
                                    {
                                        this.state.messages.successMessage &&
                                        <Alert
                                            onClick={this.clearFormMessage}
                                            type='alert-success'
                                            strong='Uspjeh!'
                                            message={this.state.messages.successMessage}
                                        />
                                    }
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col form-group col-lg-6 text-center'>
                                    <button
                                        className="btn btn-danger text-center"
                                        type="button"
                                        onClick={(e) => {
                                            this.props.closeModal();
                                        }}
                                    >
                                        Otkaži
                                    </button>

                                </div>
                                <div className='col form-group col-lg-6 text-center'>
                                    <button
                                        className="btn btn-primary text-center"
                                        type="button"
                                        onClick={(e) => {
                                            this.onAddHour(e)
                                        }}
                                    >
                                        Uredu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}
