import React from 'react';
import {getAllStudentsByClassBook} from "../admin/actions";
import UserRow from "../admin/userRow";
import uuid from "uuid";
import helpers from "../../helpers/storage.helper";

export default class Directory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            students: [],
            loaders: {
                studentLoading: true,
            },
            isClassTeacher: true,
        };
    }

    componentDidMount() {
        this.getStudents();
    }

    getStudents = () => {
        if (!helpers.getCurrentClassBook()) {
            return;
        }
        getAllStudentsByClassBook(helpers.getCurrentClassBook()._id).then(students => {
            this.setState({
                students,
                loaders: {
                    studentLoading: false,
                }
            });
        });
    };

    handleRandomChooseStudent = () => {
        if (!this.state.students.length) return;

        let random = Math.round(Math.random() * 100) % (this.state.students.length);
        this.onStudentClick(this.state.students[random]._id, random);
    };

    onStudentClick = (studentId, index) => {
        helpers.setCurrentStudentNumber(index + 1);
        this.props.history.push('/teacher/student/' + studentId);
    };

    render() {
        return (
            <div className='container container-wide'>
                <div className='row'>
                    <div className='col col-lg-10'>
                        {
                            this.state.loaders.studentLoading ?
                                <div className='text-align-center'>
                                    <div className="loader-admin-tables"/>
                                </div>
                                : <table className='table table-hover'>
                                    <thead>
                                    <tr>
                                        <th className='first-column'>#</th>
                                        <th>Učenici</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.students.map((student, index) => {
                                            return (
                                                <UserRow key={uuid()}
                                                         index={index}
                                                         user={student}
                                                         userType='student'
                                                         onClick={(e) => this.onStudentClick(student._id, index)}
                                                />
                                            );
                                        })
                                    }
                                    </tbody>
                                </table>
                        }
                    </div>
                    <div className='col col-lg-2 text-align-center'>
                        <button
                            className="btn btn-primary text-center"
                            type="button"
                            onClick={this.handleRandomChooseStudent}
                        >
                            Slučajni odabir
                        </button>
                    </div>
                </div>
            </div>
        );
    }
};