import React from 'react';
import {findTeacherById} from "./actions";

export default class ClassBookRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            teacherName: '',
            teacherId: props.classBook.teacherId
        }
    }

    componentDidMount() {
        this.getTeacher();
    }

    getTeacher = () => {
        findTeacherById(this.props.classBook.teacherId)
            .then((teacher) => {
                this.setState({
                    teacherName: teacher.firstName + ' ' + teacher.lastName,
                })
            })
    };

    render() {
        return (
            <tr className="cursor-pointer" onClick={(e) => this.props.onClick(this.props.classBook._id)}>
                <td>{this.props.index + 1}</td>
                <td className=''>{this.props.classBook.grade}</td>
                <td className=''>{this.props.classBook.class}</td>
                <td>{this.state.teacherName}</td>
            </tr>
        );
    }
};