import React from 'react';
import {getAllUsers} from "../admin/actions";
import uuid from 'uuid';
import helper from "../../helpers/storage.helper";
import WorkingHourModal from "./workingHourModal";

export default class WorkingDay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subjects: [],
            teachers: [],
            loaders: {
                dataLoading: true,
            },
            workingDay: helper.getCurrentWorkingDay(),
            workingHourModal: {
                show: false,
            },
        }

    }

    componentDidMount() {
        getAllUsers('subject')
            .then(subjects => {
                getAllUsers('teacher')
                    .then(teachers => {
                        this.setState({
                            subjects,
                            teachers,
                            loaders: {
                                dataLoading: false,
                            }
                        })
                    }).catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    }

    closeModal = () => {
        this.setState({
            workingHourModal: {
                show: false,
            },
            workingDay: helper.getCurrentWorkingDay(),
        });
    };

    handleAddHour = () => {
        helper.setCurrentWorkingDay(this.state.workingDay);
        this.setState({
            workingHourModal: {
                show: true,
            }
        })
    };

    editHour = (workingHour) => {
        console.log(workingHour);
        helper.setCurrentWorkingDay(this.state.workingDay);
        this.setState({
            editValues: {
                content: workingHour.content,
                note: workingHour.note,
                hour: workingHour.hour,
            },
            workingHourModal: {
                show: true,
            }
        })
    };

    render() {
        let workingHoursArray = [];

        for (let i = 0; i < 7; i++) {
            let subject = null;
            let teacher = null;

            if (this.state.workingDay.workingHours[i].subjectId) {
                this.state.subjects.forEach(s => {
                    if (s._id === this.state.workingDay.workingHours[i].subjectId) {
                        subject = s;
                    }
                });
                this.state.teachers.forEach(t => {
                    if (subject && t._id === subject.teacherId) {
                        teacher = t;
                    }
                });
            }

            workingHoursArray.push({
                hour: i + 1,
                subjectAndSchoolHourNumber: this.state.workingDay.workingHours[i].workingHourNumber ? (subject ? subject.name : '') + ' {' + this.state.workingDay.workingHours[i].workingHourNumber + '} ' : '',
                teacher: teacher ? teacher.firstName + ' ' + teacher.lastName : '',
                content: this.state.workingDay.workingHours[i].content,
                note: this.state.workingDay.workingHours[i].note,
            });
        }
        return (
            <div className='container container-wide'>
                <div className='row'>
                    <div className='col-lg-6 float-left'>
                        <h3>
                            {this.state.workingDay.workingDayNumber + '. radni dan'}
                        </h3>
                    </div>
                    <div className='col-lg-6'>
                        <h3 className={'float-right'}>
                            {new Date(this.state.workingDay.date).toLocaleDateString('sr')}
                        </h3>
                    </div>
                </div>
                <div className='row'>
                    <table className='table working-week-table'>
                        <thead>
                        <tr>
                            <th>Čas</th>
                            <th>Predmet i broj nastavnog časa</th>
                            <th>Nastavnik</th>
                            <th>Sadržaj</th>
                            <th>Napomena</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            workingHoursArray.map(w => {
                                return (
                                    <tr key={uuid()}>
                                        <td className='working-week-cell-border'>
                                            {w.hour + '.'}
                                            <span className="glyphicon glyphicon-edit pull-right"
                                                  aria-hidden="true"
                                                  onClick={() => this.editHour(w)}
                                            />
                                        </td>
                                        <td className='working-week-cell-border'>
                                            {w.subjectAndSchoolHourNumber}
                                        </td>
                                        <td className='working-week-cell-border'>
                                            {w.teacher}
                                        </td>
                                        <td className='working-week-cell-border'>
                                            {w.content}
                                        </td>
                                        <td className='working-week-cell-border'>
                                            {w.note}
                                        </td>
                                    </tr>
                                );
                            })
                        }
                        </tbody>
                    </table>
                </div>

                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='col form-group col-lg-12 text-center'>
                            <button
                                className="btn btn-primary text-center"
                                type="button"
                                onClick={(e) => this.handleAddHour(e)}
                            >
                                Dodaj čas
                            </button>
                        </div>
                    </div>
                </div>
                <WorkingHourModal
                    workingHourModal={this.state.workingHourModal}
                    subjects={this.state.subjects}
                    closeModal={this.closeModal}
                />
            </div>
        );
    }
}