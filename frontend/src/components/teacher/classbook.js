import React from 'react';
import {getAllUsers, getUserById} from "../admin/actions";
import helpers from '../../helpers/storage.helper';
import uuid from "uuid";
import ClassBookRow from "./classBookRow";
import {findTeacherById} from "./actions";
import Alert from "../alert/alert";

export default class Classbook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            teacher: {},
            classBooks: [],
            loaders: {
                classBookLoading: true,
            },
            messages: {},
        };
    }

    componentDidMount() {
        let currentUser = helpers.getCurrentUser();
        findTeacherById(currentUser.id)
            .then(teacher => {
                this.setState({
                    teacher,
                })
            }).then(() => {
            getAllUsers('classBook')
                .then((classBooks) => {
                    this.setState({
                        classBooks,
                        loaders: {
                            classBookLoading: false,
                        }
                    })
                })
        })
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    handleClassBookClick = (classBookId) => {
        getUserById('classBook', classBookId)
            .then((classBook) => {
                helpers.setCurrentClassBook(classBook);
                getUserById('teacher', classBook.teacherId)
                    .then(teacher => {
                        // Current user is class teacher of that classBook
                        if (helpers.getCurrentUser().id === teacher._id) {
                            helpers.setIsClassTeacherToTrue();
                        } else {
                            helpers.removeIsClassTeacherTrue();
                        }
                        this.setState({
                            messages: {
                                successMessage: `Odabrali ste odjeljensku knjigu razrednika ${teacher.firstName} ${teacher.lastName} ${classBook.grade}-${classBook.class}`
                            }
                        })
                    })
            }).catch(err => console.log(err));
    };

    render() {
        return (
            <div className='wrapper'>
                <h2 className='text-align-center'>{`Dobrodošli ${this.state.teacher.firstName} ${this.state.teacher.lastName}`}</h2>
                <h3 className='text-align-center'>Odaberite odjeljensku knjigu</h3>
                <div className='container'>
                    <div className='row'>
                        <div className='col col-lg-12'>
                            {
                                this.state.loaders.classBookLoading ?
                                    <div className='text-align-center'>
                                        <div className="loader-admin-tables"/>
                                    </div>
                                    : <table className='table table-hover'>
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Razred</th>
                                            <th>Odjeljenje</th>
                                            <th>Razrednik</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.classBooks.map((classBook, index) => {
                                                return (
                                                    <ClassBookRow key={uuid()}
                                                                  index={index}
                                                                  classBook={classBook}
                                                                  onClick={(e) => this.handleClassBookClick(e)}
                                                    />
                                                );
                                            })
                                        }
                                        </tbody>
                                    </table>
                            }

                        </div>

                    </div>
                    {
                        this.state.messages.successMessage &&
                        <div className='padding-left'>
                            <Alert
                                onClick={this.clearFormMessage}
                                type='alert-success'
                                strong='Uspjeh!'
                                message={this.state.messages.successMessage}
                            />
                        </div>
                    }
                </div>
            </div>
        );
    }
};