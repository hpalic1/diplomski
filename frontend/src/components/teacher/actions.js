import {baseURL} from "../../config";
import helpers from '../../helpers/storage.helper';

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    let error = new Error(`Http Error ${response.statusText}`);
    error.status = response.status;
    error.respones = response;
    console.log(error);
    throw error;
}

export const addClassBook = (data) => {
    return new Promise(function (resolve, reject) {
        let message = {
            successMessage: '',
            errorMessage: '',
        };
        fetch(`${baseURL}/classBook`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(() => {
                message.successMessage = 'Uspješno dodana odjeljenska knjiga';
                resolve(message);
            }).catch(err => {
            message.errorMessage = 'Desila se greška';
            reject(message);
        });
    });
};

export const findTeacherById = (teacherId) => {
    let message = {
        successMessage: '',
        errorMessage: '',
    };
    return new Promise((resolve, reject) => {
        fetch(`${baseURL}/teacher/id/${teacherId}`, {
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                message.errorMessage = 'Desila se greška';
                reject(message);
            })
    });
};

export const updateClassBook = (data) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/classBook`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};

export const addWorkingHour = (data) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/classBook/workingHour`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};

export const markStudent = (data) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/student/mark`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};

export const editBehavior = (data) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/student/behavior`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};

export const addParentMeeting = (data) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/parentMeeting`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};