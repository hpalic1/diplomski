import React from 'react';
import {getUserById} from "../admin/actions";
import helpers from "../../helpers/storage.helper";
import Alert from "../alert/alert";
import uuid from 'uuid';
import {updateClassBook} from "./actions";
import {addDays, subtractDays} from "../../helpers/date.helper";

export default class Registry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            classBook: {},
            loaders: {
                classBookLoading: true,
            },
            messages: {
                successMessage: '',
                errorMessage: '',
            },
        };
    }

    componentDidMount() {
        if (!helpers.getCurrentClassBook()) {
            return;
        }
        getUserById('classBook', helpers.getCurrentClassBook()._id)
            .then(classBook => {
                this.setState({
                    classBook,
                    loaders: {
                        classBookLoading: false,
                    }
                })
            }).catch(err => console.log(err));
    }

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    handleDayClick = (workingDayNumber, workingWeekNumber) => {
        let day = this.state.classBook.workingWeeks[workingWeekNumber - 1].workingDays[(workingDayNumber - 1) % 5];
        helpers.setCurrentWorkingDay(day);
        this.props.history.push('/teacher/workingDay')
    };

    handleAddWorkingWeek = (e) => {
        let classBook = JSON.parse(JSON.stringify(this.state.classBook));

        let days = [];


        let lastAddedDay = null;
        if (classBook.workingWeeks.length > 0) {
            lastAddedDay = new Date(classBook.workingWeeks[classBook.workingWeeks.length - 1].workingDays[4].date);
        }

        for (let i = 0; i < 5; i++) {
            let date = new Date();

            if (!lastAddedDay) {
                date = subtractDays(date, date.getDay() - 1);
            } else {
                date = addDays(date, 3);
            }

            date = addDays(date, i);

            let hours = [];

            for (let j = 0; j < 7; j++) {
                hours.push({
                    subjectId: null,
                    schoolHourNumber: j + 1,
                    note: '',
                    content: '',
                    workingHourNumber: null,
                })
            }

            date = JSON.parse(JSON.stringify(date));

            days.push({
                workingHours: hours,
                workingDayNumber: this.state.classBook.workingWeeks.length * 5 + i + 1,
                date: date
            });
        }

        classBook.workingWeeks.push({
            workingDays: days,
            workingWeekNumber: this.state.classBook.workingWeeks.length + 1,
        });
        let data = {
            week: classBook.workingWeeks[classBook.workingWeeks.length - 1],
            id: classBook._id,
        };
        updateClassBook(data)
            .then(result => {
                console.log('classBook', result);
                helpers.setCurrentClassBook(result);
                this.setState({
                    classBook: result,
                    messages: {
                        successMessage: 'Uspješno dodana radna sedmica'
                    }
                })
            })
            .catch(err => console.log(err));

    };

    render() {
        return (
            <div className='container container-wide'>
                {
                    this.state.loaders.classBookLoading ?
                        <div className='text-align-center'>
                            <div className="loader-admin-tables"/>
                        </div>
                        :
                        <table className='table working-week-table'>
                            <tbody>
                            {
                                this.state.classBook.workingWeeks.map((workingWeek, index) => {
                                    return (
                                        <tr key={uuid()}>
                                            <td className='working-week-cell-border'>
                                                {this.state.classBook.workingWeeks[index].workingWeekNumber + '. Radna sedmica'}
                                            </td>
                                            {
                                                workingWeek.workingDays.map((workingDay, index) => {
                                                    let nameOfDay = '';
                                                    switch (index) {
                                                        case 0:
                                                            nameOfDay = 'Ponedjeljak';
                                                            break;
                                                        case 1:
                                                            nameOfDay = 'Utorak';
                                                            break;
                                                        case 2:
                                                            nameOfDay = 'Srijeda';
                                                            break;
                                                        case 3:
                                                            nameOfDay = 'Četvrtak';
                                                            break;
                                                        case 4:
                                                            nameOfDay = 'Petak';
                                                            break;
                                                        default :
                                                            nameOfDay = 'Error';
                                                    }
                                                    return (
                                                        <td className='cursor-pointer table-cell-hover working-week-cell-border'
                                                            key={uuid()}
                                                            onClick={() => this.handleDayClick(workingDay.workingDayNumber, workingWeek.workingWeekNumber)}>
                                                            <div>
                                                                {nameOfDay + ' (' + workingDay.workingDayNumber + ')'}
                                                            </div>
                                                            <div>
                                                                {new Date(workingDay.date).toLocaleDateString('sr')}
                                                            </div>
                                                        </td>
                                                    );
                                                })
                                            }
                                        </tr>
                                    );
                                })
                            }
                            </tbody>
                        </table>

                }
                {
                    this.state.messages.successMessage &&
                    <div className='padding-left'>
                        <Alert
                            onClick={this.clearFormMessage}
                            type='alert-success'
                            strong='Uspjeh!'
                            message={this.state.messages.successMessage}
                        />
                    </div>
                }
                <div className='row'>
                    {
                        helpers.getIsClassTeacherTrue() ?
                            <div className='col-lg-12'>
                                <div className='col form-group col-lg-12 text-center'>
                                    <button
                                        className="btn btn-primary text-center"
                                        type="button"
                                        onClick={(e) => this.handleAddWorkingWeek(e)}
                                    >
                                        Dodaj radnu sedmicu
                                    </button>
                                </div>
                            </div> : null
                    }
                </div>
            </div>
        );
    }
};