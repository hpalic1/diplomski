import React from 'react';
import InputField from "../inputField/inputField";
import {Validator} from "./validation";
import NumericInput from 'react-numeric-input';
import {addSubject, editSubject, getAllSubjects, getAllUsers} from "./actions";
import Alert from "../alert/alert";
import uuid from "uuid";

import SubjectRow from "./subjectRow";
import {PanelGroup} from "react-bootstrap";


class Subjects extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            subjects: [],
            teachers: [],
            fields: {
                name: '',
                description: '',
                literature: '',
                numberOfHours: '',
            },
            fieldErrors: {},
            subjectMessages: {},
            teacherMessages: {},
            loaders: {
                subjectsLoading: true,
            },
            selectedTeacherId: null,
            selectedTeacher: '',
            selectedSubjectId: null,
            selectedSubject: ''
        }
    }

    componentDidMount() {
        this.renderSubjects();
        getAllUsers('teacher')
            .then((teachers) => {
                this.setState({
                    teachers: teachers
                }, () => {
                    // defaultValue={this.state.teachers.length !== 0 ? this.state.teachers[0] : null}
                    if (this.state.teachers.length !== 0) {
                        this.setState({
                            selectedTeacherId: this.state.teachers[0]._id,
                            selectedTeacher: this.state.teachers[0].firstName + ' ' + this.state.teachers[0].lastName,
                        })
                    }
                })
            });
    }

    onChange = (name, value, error) => { // Must use onChange like this or InputField component won't work

        const fields = JSON.parse(JSON.stringify(this.state.fields));
        const fieldErrors = Object.assign({}, this.state.fieldErrors);

        fields[name] = value;
        fieldErrors[name] = error;
        this.setState({
            fields,
            fieldErrors,
        });
    };

    onNumberOfHoursChange = (value) => {
        const fields = JSON.parse(JSON.stringify(this.state.fields));
        fields.numberOfHours = value;
        this.setState({
            fields,
        })
    };

    onTextAreaChange = (e) => {
        const fields = JSON.parse(JSON.stringify(this.state.fields));
        fields[e.target.name] = e.target.value;
        this.setState({
            fields,
        })
    };

    onAddSubject = () => {
        let messages = {};

        if (!Validator.validateSubjectForm(this.state.fields, this.state.fieldErrors)) {
            messages.errorMessage = 'Forma nije validna, ispravite greške i pokušajte ponovo';
            this.setState({
                subjectMessages: messages,
            });
            return;
        }
        addSubject(this.state.fields)
            .then(message => this.setState({
                subjectMessages: message
            }))
            .then(() => {
                this.setState({
                    fields: {
                        name: '',
                        description: '',
                        literature: '',
                        numberOfHours: '',
                    },
                })
            }).then(() => this.renderSubjects())
            .catch(err => {
                console.error(err);
            })
    };

    clearSubjectFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            subjectMessages: {}
        })
    };

    clearTeacherFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            teacherMessages: {}
        })
    };

    renderSubjects = () => {
        getAllSubjects()
            .then((subjects) => {
                this.setState({
                    subjects,
                    loaders: {
                        subjectsLoading: false,
                    }
                },() => {
                    if (this.state.subjects.length !== 0) {
                        this.setState({
                            selectedSubjectId: this.state.subjects[0]._id,
                            selectedSubject: this.state.subjects[0].name,
                        })
                    }
                })
            })
    };

    onSelectTeacherChange = (e) => {
        this.setState({
            selectedTeacherId: this.state.teachers[e.nativeEvent.target.selectedIndex]._id,
            selectedTeacher: this.state.teachers[e.nativeEvent.target.selectedIndex].firstName.concat(' ', this.state.teachers[e.nativeEvent.target.selectedIndex].lastName)
        })
    };

    onSelectSubjectChange = (e) => {
        this.setState({
            selectedSubjectId: this.state.subjects[e.nativeEvent.target.selectedIndex]._id,
            selectedSubject: this.state.subjects[e.nativeEvent.target.selectedIndex].name
        })
    };

    onAssignSubject = () => {
        let data = {
            subjectId: this.state.selectedSubjectId,
            teacherId: this.state.selectedTeacherId,
        };

        if (!data.subjectId || !data.teacherId) {
            this.setState({
                teacherMessages: {
                    errorMessage: 'Dogodila se greška'
                },
            });
            return;
        }

        editSubject(data)
            .then((editedSubject) => {
                this.setState({
                    teacherMessages: {
                        successMessage: `Uspješno dodijeljen nastavnik predmetu`
                    },
                })
            })
            .catch((err) => {
                console.error(err);
            })
    };

    render() {
        return (
            <div className='container container-wide'>
                <div className='row'>
                    <div className='col-lg-4'>
                        <form>
                            <div className='form-group col-lg-12'>
                                <InputField
                                    label='Naziv predmeta'
                                    type='text'
                                    name='name'
                                    placeholder='Unesi naziv'
                                    value={this.state.fields.name}
                                    onChange={this.onChange}
                                    validate={Validator.validateSubjectName}
                                    error={this.state.fieldErrors.name}
                                    errorMessage='Naziv predmeta mora imati između 3 i 20 slova i započinjati velikim slovom'
                                />
                            </div>
                            <div className='form-group col-lg-12'>
                                <label>Broj časova</label>
                                <NumericInput
                                    className='form-control'
                                    min={1}
                                    max={200}
                                    value={this.state.fields.numberOfHours}
                                    strict
                                    onChange={(e) => this.onNumberOfHoursChange(e)}
                                />
                            </div>
                            <div className='form-group col-lg-12'>
                                <label>Opis predmeta</label>
                                <textarea
                                    className='form-control resize-none'
                                    rows={6}
                                    name='description'
                                    value={this.state.fields.description}
                                    onChange={(e) => this.onTextAreaChange(e)}
                                >
                                </textarea>
                            </div>
                            <div className='form-group col-lg-12'>
                                <label>Literatura</label>
                                <textarea
                                    className='form-control resize-none'
                                    rows={4}
                                    name='literature'
                                    value={this.state.fields.literature}
                                    onChange={(e) => this.onTextAreaChange(e)}
                                >
                                </textarea>
                            </div>
                            <div className='col form-group col-lg-12 text-center'>
                                <button
                                    className="btn btn-primary text-center"
                                    type="button"
                                    onClick={this.onAddSubject}
                                >
                                    Dodaj predmet
                                </button>
                            </div>
                        </form>
                        <div className='col-lg-12 form-group'>
                            {
                                this.state.subjectMessages.errorMessage &&
                                <Alert
                                    onClick={this.clearSubjectFormMessage}
                                    type='alert-danger'
                                    strong='Problem!'
                                    message={this.state.subjectMessages.errorMessage}
                                />
                            }
                            {
                                this.state.subjectMessages.successMessage &&
                                <Alert
                                    onClick={this.clearSubjectFormMessage}
                                    type='alert-success'
                                    strong='Uspjeh!'
                                    message={this.state.subjectMessages.successMessage}
                                />
                            }
                        </div>
                    </div>
                    <div className='col-lg-4'>
                        <label>Predmeti</label>
                        {
                            this.state.loaders.subjectsLoading ?
                                <div className='text-align-center'>
                                    <div className="loader-admin-tables"/>
                                </div> :
                                <PanelGroup accordion id="accordion">
                                    {
                                        this.state.subjects.map((subject, index) => {
                                            return (
                                                <SubjectRow
                                                    key={uuid()}
                                                    index={index}
                                                    subject={subject}
                                                />
                                            )
                                        })
                                    }
                                </PanelGroup>
                        }
                    </div>
                    <div className='col-lg-4'>
                        <label>Odaberi nastavnika</label>
                        <select className='form-control'
                                onChange={(e) => this.onSelectTeacherChange(e)}
                                value={this.state.selectedTeacher}
                        >
                            {
                                this.state.teachers.map((teacher) => {
                                    return (
                                        <option key={uuid()}>
                                            {teacher.firstName.concat(' ', teacher.lastName)}
                                        </option>
                                    );
                                })
                            }
                        </select>
                        <br/>
                        <label>Odaberi predmet</label>
                        <select className='form-control'
                                onChange={(e) => this.onSelectSubjectChange(e)}
                                value={this.state.selectedSubject}
                        >
                            {
                                this.state.subjects.map((subject) => {
                                    return (
                                        <option key={uuid()}>
                                            {subject.name}
                                        </option>
                                    );
                                })
                            }
                        </select>
                        <br/>
                        <div className='col-lg-12 form-group'>
                            {
                                this.state.teacherMessages.errorMessage &&
                                <Alert
                                    onClick={this.clearTeacherFormMessage}
                                    type='alert-danger'
                                    strong='Problem!'
                                    message={this.state.teacherMessages.errorMessage}
                                />
                            }
                            {
                                this.state.teacherMessages.successMessage &&
                                <Alert
                                    onClick={this.clearTeacherFormMessage}
                                    type='alert-success'
                                    strong='Uspjeh!'
                                    message={this.state.teacherMessages.successMessage}
                                />
                            }
                        </div>
                        <div className='text-align-center'>
                            <button
                                className="btn btn-primary "
                                type="button"
                                onClick={this.onAssignSubject}
                            >
                                Dodaj predmet
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Subjects;
