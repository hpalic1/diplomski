import React from 'react';
import {Panel} from "react-bootstrap";

class SubjectRow extends React.Component {
    render() {
        return (
            <Panel eventKey={this.props.index + 1}>
                <Panel.Heading>
                    <Panel.Title className='panel-font' toggle>{this.props.subject.name}</Panel.Title>
                </Panel.Heading>
                <Panel.Body collapsible className='panel-body'>
                    <h5 className='panel-body-heading'>Opis predmeta</h5>
                    <span>{this.props.subject.description}</span>
                    <h5 className='panel-body-heading'>Literatura</h5>
                    <span>{this.props.subject.literature}</span>
                    <h5 className='panel-body-heading'>Broj časova</h5>
                    <span>{this.props.subject.numberOfHours}</span>
                </Panel.Body>
            </Panel>
        );
    }
}

export default SubjectRow;