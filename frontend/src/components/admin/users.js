import React from 'react';
import {getAllUsers} from "./actions";
import UserRow from "./userRow";
import uuid from 'uuid';
import UserModal from "../userModal/userModal";
import Alert from "../alert/alert";

class Users extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            students: [],
            teachers: [],
            parents: [],
            fields: {
                firstName: '',
                lastName: '',
                birthdate: '',
                address: '',
                umcn: '',
                phone: '',
                username: '',
                sex: 'M',
                password: '',
                confirmPassword: '',
            },
            fieldErrors: [],
            messages: [],
            userModal: {
                show: false,
                userType: '',
                user: {},
                id: null,
            },
            loaders: {
                parentLoading: true,
                studentLoading: true,
                teacherLoading: true,
            }
        };
    }

    componentDidMount() {
        this.renderUsers();
    }

    renderUsers = () => {
        getAllUsers('student').then(students => {
            this.setState({
                students,
                loaders: {
                    parentLoading: this.state.loaders.parentLoading,
                    studentLoading: false,
                    teacherLoading: this.state.loaders.teacherLoading,
                }
            });
        }).catch(err => {
            console.log('loading students', err);
        });
        getAllUsers('teacher').then(teachers => {
            this.setState({
                teachers,
                loaders: {
                    parentLoading: this.state.loaders.parentLoading,
                    studentLoading: this.state.loaders.studentLoading,
                    teacherLoading: false,
                }
            });
        }).catch(err => {
            console.log(err);
        });
        getAllUsers('parent').then(parents => {
            this.setState({
                parents,
                loaders: {
                    parentLoading: false,
                    studentLoading: this.state.loaders.studentLoading,
                    teacherLoading: this.state.loaders.teacherLoading,
                }
            });
        }).catch(err => {
            console.log(err);
        });
    };

    onUserRowClick = (id, userType, user) => {

        this.setState({
            userModal: {
                show: true,
                userType: userType,
                user: user,
                id,
            },

        })
    };

    closeModal = () => {
        this.setState({
            userModal: {
                show: false,
                userType: '',
                user: {},
            },
        })
    };

    onEditSuccess = (userData) => {
        this.setState({
            userModal: {
                show: false,
                userType: '',
                user: {},
                id: null,
            },
            messages: {
                successMessage: `Uspješno izmjenjen korisnik: ${userData.firstName} ${userData.lastName}`
            },

        }, () => {
            this.renderUsers();
        })
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    render() {
        return (
            <div className='container-wide'>
                <div className='row'>
                    <div className='col col-lg-4'>
                        {
                            this.state.loaders.studentLoading ?
                                <div className='text-align-center'>
                                    <div className="loader-admin-tables"/>
                                </div>
                                : <table className='table table-hover'>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Učenici</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.students.map((student, index) => {
                                            return (
                                                <UserRow key={uuid()}
                                                         index={index}
                                                         user={student}
                                                         userType='student'
                                                         onClick={this.onUserRowClick}
                                                />
                                            );
                                        })
                                    }
                                    </tbody>
                                </table>
                        }
                    </div>
                    <div className='col col-lg-4'>
                        {
                            this.state.loaders.parentLoading ?
                                <div className='text-align-center'>
                                    <div className="loader-admin-tables"/>
                                </div>
                                : <table className='table table-hover'>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Roditelji</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {


                                        this.state.parents.map((parent, index) => {
                                            return (
                                                <UserRow key={uuid()}
                                                         index={index}
                                                         user={parent}
                                                         userType='parent'
                                                         onClick={this.onUserRowClick}
                                                />
                                            );
                                        })
                                    }
                                    </tbody>
                                </table>
                        }

                    </div>
                    <div className='col col-lg-4'>
                        {
                            this.state.loaders.teacherLoading ?
                                <div className='text-align-center'>
                                    <div className="loader-admin-tables"/>
                                </div>
                                : <table className='table table-hover'>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nastavnici</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.teachers.map((teacher, index) => {
                                            return (
                                                <UserRow key={uuid()}
                                                         index={index}
                                                         user={teacher}
                                                         userType='teacher'
                                                         onClick={this.onUserRowClick}
                                                />
                                            );
                                        })
                                    }
                                    </tbody>
                                </table>
                        }

                    </div>
                </div>
                {
                    this.state.messages.successMessage &&
                    <div className='padding-left'>
                        <Alert
                            onClick={this.clearFormMessage}
                            type='alert-success'
                            strong='Uspjeh!'
                            message={this.state.messages.successMessage}
                        />
                    </div>
                }
                <UserModal userModal={this.state.userModal}
                           closeModal={this.closeModal}
                           editSuccess={this.onEditSuccess}
                />
            </div>
        );
    }
}

export default Users;