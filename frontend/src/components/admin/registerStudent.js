import React from 'react';
import InputField from "../inputField/inputField";
import {Validator} from "./validation";
import Datetime from "react-datetime";
import Alert from "../alert/alert";
import {generateUsername, resetFields} from "./helper";
import {getAllUsers, registerParent, registerStudent} from "./actions";
import moment from "moment";
import uuid from "uuid";

class RegisterStudent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {
                student: {
                    firstName: '',
                    lastName: '',
                    birthdate: '',
                    address: '',
                    umcn: '',
                    phone: '',
                    username: '',
                    sex: 'M',
                    password: '',
                    confirmPassword: '',
                },
                parent: {
                    firstName: '',
                    lastName: '',
                    birthdate: '',
                    address: '',
                    umcn: '',
                    phone: '',
                    username: '',
                    sex: 'M',
                    password: '',
                    confirmPassword: '',
                }
            },
            messages: {},
            studentFieldErrors: {},
            parentFieldErrors: {},
            selectedClassBookId: null,
            selectedClassBook: '',
            classBooks: [],

        };
    }

    componentDidMount() {
        getAllUsers('classBook')
            .then((classBooks) => {
                this.setState({
                    classBooks: classBooks
                }, () => {
                    if (this.state.classBooks.length !== 0) {
                        this.setState({
                            selectedClassBookId: this.state.classBooks[0]._id,
                            selectedClassBook: this.state.classBooks[0].grade + '-' + this.state.classBooks[0].class,
                        })
                    }
                })
            }).catch(err => {
            console.error(err);
        });
    }

    onStudentChange = (name, value, error) => { // Must use onChange like this or InputField component won't work
        const fields = JSON.parse((JSON.stringify(this.state.fields)));
        const studentFieldErrors = Object.assign({}, this.state.studentFieldErrors);

        fields.student[name] = value;
        studentFieldErrors[name] = error;
        this.setState({
            fields,
            studentFieldErrors,
        });

    };

    onParentChange = (name, value, error) => { // Must use onChange like this or InputField component won't work
        let fields = JSON.parse((JSON.stringify(this.state.fields)));

        let parentFieldErrors = Object.assign({}, this.state.parentFieldErrors);

        fields.parent[name] = value;
        parentFieldErrors[name] = error;
        this.setState({
            fields,
            parentFieldErrors,
        });

    };

    onStudentBirthdateChange = (date) => {
        // date je objekat tima Moment
        const studentFieldErrors = Object.assign({}, this.state.studentFieldErrors);
        if (!Validator.validateDateFormat(date)) {
            studentFieldErrors.birthdate = 'Neispravan datum';
        } else {
            studentFieldErrors.birthdate = '';
        }
        let fields = JSON.parse((JSON.stringify(this.state.fields)));
        fields.student.birthdate = date;
        this.setState({
            fields,
            studentFieldErrors,
        });
    };

    onParentBirthdateChange = (date) => {
        // date je objekat tima Moment
        const parentFieldErrors = Object.assign({}, this.state.parentFieldErrors);
        if (!Validator.validateDateFormat(date)) {
            parentFieldErrors.birthdate = 'Neispravan datum';
        } else {
            parentFieldErrors.birthdate = '';
        }
        let fields = JSON.parse((JSON.stringify(this.state.fields)));
        fields.parent.birthdate = date;
        this.setState({
            fields,
            parentFieldErrors,
        });
    };

    onStudentSexChange = (evt) => {
        let fields = JSON.parse((JSON.stringify(this.state.fields)));
        fields.student.sex = evt.target.value;
        this.setState({
            fields,
        });
    };

    onParentSexChange = (evt) => {
        let fields = JSON.parse((JSON.stringify(this.state.fields)));
        fields.parent.sex = evt.target.value;
        this.setState({
            fields,
        });
    };

    onStudentConfirmPasswordChange = (e) => {
        let fields = JSON.parse((JSON.stringify(this.state.fields)));
        fields.student.confirmPassword = e.target.value;
        this.setState({
            fields,
        })
    };

    onParentConfirmPasswordChange = (e) => {
        let fields = JSON.parse((JSON.stringify(this.state.fields)));
        fields.parent.confirmPassword = e.target.value;
        this.setState({
            fields,
        })
    };

    handleStudentGenerateUsername = (e) => {
        e.preventDefault();

        let messages = {};

        if (!this.state.fields.student.firstName || !this.state.fields.student.lastName) {
            messages.errorMessage = 'Ime i prezime studenta ne može biti prazno';
            this.setState({
                messages,
            });
            return;
        }

        generateUsername(this.state.fields.student.firstName, this.state.fields.student.lastName)
            .then(u => {
                let fields = JSON.parse((JSON.stringify(this.state.fields)));
                fields.student.username = u;
                this.setState({
                    fields,
                });
            })
            .catch(err => {
                console.error('Error generating username', err);
                messages.errorMessage = 'Greška pri kreiranju studentskog korisničkog imena';
                this.setState({
                    messages,
                })
            });
    };

    handleParentGenerateUsername = (e) => {
        e.preventDefault();

        let messages = {};

        if (!this.state.fields.parent.firstName || !this.state.fields.parent.lastName) {
            messages.errorMessage = 'Ime i prezime roditelja ne može biti prazno';
            this.setState({
                messages,
            });
            return;
        }

        generateUsername(this.state.fields.parent.firstName, this.state.fields.parent.lastName)
            .then(u => {
                let fields = JSON.parse((JSON.stringify(this.state.fields)));
                fields.parent.username = u;
                this.setState({
                    fields,
                });
            })
            .catch(err => {
                console.error('Error generating username', err);
                messages.errorMessage = 'Greška pri kreiranju roditeljskog korisničkog imena';
                this.setState({
                    messages,
                })
            });
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    onSelectClassBookChange = (e) => {
        this.setState({
            selectedClassBookId: this.state.classBooks[e.nativeEvent.target.selectedIndex]._id,
            selectedClassBook: this.state.classBooks[e.nativeEvent.target.selectedIndex].grade + '-' + this.state.classBooks[e.nativeEvent.target.selectedIndex].class,
        })
    };

    onRegisterStudent = (evt) => {
        evt.preventDefault();

        let messages = {};

        if (!Validator.validateForm(this.state.fields.student, this.state.studentFieldErrors) || !Validator.validateForm(this.state.fields.parent, this.state.parentFieldErrors)) {
            messages.errorMessage = 'Forma nije validna, ispravite greške i pokušajte ponovo';
            this.setState({
                messages,
            });
            return;
        }

        if (this.state.fields.student.password !== this.state.fields.student.confirmPassword) {
            messages.errorMessage = 'Lozinke studenta se ne podudaraju';
            this.setState({
                messages,
            });
            return;
        }
        if (this.state.fields.parent.password !== this.state.fields.parent.confirmPassword) {
            messages.errorMessage = 'Lozinke roditelja se ne podudaraju';
            this.setState({
                messages,
            });
            return;
        }

        if (!this.state.selectedClassBookId) {
            messages.errorMessage = 'Odjeljenska knjiga mora postojati';
            this.setState({
                messages,
            });
            return;
        }

        const data = JSON.parse((JSON.stringify(this.state.fields)));

        data.student.classBookId = this.state.selectedClassBookId;

        delete data.student.confirmPassword;
        delete data.parent.confirmPassword;

        registerParent(data.parent)
            .then(message => {
                this.setState({
                    messages: message,
                });
                data.student.parentId = message.appendedParentId;
                registerStudent(data.student)
                    .then(message => this.setState({
                        messages: message,
                    }))
                    .then(() => {
                        let fields = {
                            student: resetFields(),
                            parent: resetFields(),
                        };
                        this.setState({
                            fields,
                        });
                    });
            })
            .catch(message => this.setState({
                messages: message,
            }));
    };

    render() {
        return (
            <div className='container container-wide'>
                <form>
                    <div className='row'>
                        <div className='col-lg-6 col-md-6 col-sm-6 separate-forms'>
                            <div className='text-align-center'>
                                <h4>Unesite podatke o učeniku</h4>
                                <br/>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Ime'
                                        type='text'
                                        name='firstName'
                                        placeholder='Unesi ime'
                                        value={this.state.fields.student.firstName}
                                        onChange={this.onStudentChange}
                                        validate={Validator.validateFirstName}
                                        error={this.state.studentFieldErrors.firstName}
                                        errorMessage='Ime mora imati između 3 i 20 slova i započinjati velikim slovom'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Prezime'
                                        type='text'
                                        name='lastName'
                                        placeholder='Unesi prezime'
                                        value={this.state.fields.student.lastName}
                                        onChange={this.onStudentChange}
                                        validate={Validator.validateLastName}
                                        error={this.state.studentFieldErrors.lastName}
                                        errorMessage='Prezime mora imati između 3 i 20 slova i započinjati velikim slovom'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor='birthdate'>Datum rođenja</label>
                                    <Datetime
                                        inputProps={{
                                            placeholder: 'Odaberi datum',
                                            style: {borderColor: this.state.studentFieldErrors.birthdate ? 'red' : null}
                                        }}
                                        timeFormat={false}
                                        name='birthdate'
                                        onChange={this.onStudentBirthdateChange}
                                        value={moment(this.state.fields.student.birthdate)}
                                    />
                                    <span style={{color: 'red'}}>{this.state.studentFieldErrors.birthdate}</span>
                                    <br/>
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Adresa'
                                        type='text'
                                        name='address'
                                        placeholder='Unesi adresu'
                                        value={this.state.fields.student.address}
                                        onChange={this.onStudentChange}
                                        validate={Validator.validateAddress}
                                        error={this.state.studentFieldErrors.address}
                                        errorMessage='Adresa mora imati najmanje 3 slova'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Jedinstveni matični broj građanina'
                                        type='text'
                                        name='umcn'
                                        placeholder='Unesi JMBG'
                                        value={this.state.fields.student.umcn}
                                        onChange={this.onStudentChange}
                                        validate={Validator.validateUmcn}
                                        error={this.state.studentFieldErrors.umcn}
                                        errorMessage='JMBG mora imati 13 cifara'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Telefon'
                                        type='text'
                                        name='phone'
                                        placeholder='Unesi broj telefona'
                                        value={this.state.fields.student.phone}
                                        onChange={this.onStudentChange}
                                        validate={Validator.validatePhone}
                                        error={this.state.studentFieldErrors.phone}
                                        errorMessage='Neispravan telefon'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor='umcn'>Generisano korisničko ime</label>
                                    <input
                                        type='text'
                                        name='username'
                                        className='form-control'
                                        readOnly={true}
                                        value={this.state.fields.student.username}
                                        style={{backgroundColor: '#f7f9fc'}}
                                    />
                                    <br/>
                                </div>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor="phone"> Spol </label>
                                    <select
                                        name='sex'
                                        className='form-control'
                                        placeholder='Odaberi spol'
                                        value={this.state.fields.student.sex}
                                        onChange={this.onStudentSexChange}
                                    >
                                        <option value='M'>Muški</option>
                                        <option value='F'>Ženski</option>
                                    </select>
                                    <br/>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Lozinka'
                                        type='password'
                                        name='password'
                                        placeholder='Unesi lozinku'
                                        value={this.state.fields.student.password}
                                        onChange={this.onStudentChange}
                                        validate={Validator.validatePassword}
                                        error={this.state.studentFieldErrors.password}
                                        errorMessage='Lozinka mora imati više od 6 znakova'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor="confirmPassword"> Potvrdi lozinku </label>
                                    <input
                                        type='password'
                                        name='confirmPassword'
                                        className='form-control'
                                        placeholder='potvrdi lozinku'
                                        value={this.state.fields.student.confirmPassword}
                                        onChange={this.onStudentConfirmPasswordChange}
                                    />
                                    <br/>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-12'>
                                    <label>Odjeljenska knjiga</label>
                                    <select className='form-control'
                                            onChange={(e) => this.onSelectClassBookChange(e)}
                                            value={this.state.selectedClassBook}
                                    >
                                        {
                                            this.state.classBooks.map((classBook) => {
                                                return (
                                                    <option key={uuid()}>
                                                        {/*TODO implement teacher name after class and grade*/}
                                                        {classBook.grade + '-' + classBook.class}
                                                    </option>
                                                );
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='col form-group col-lg-12 text-center'>
                                    <button
                                        className="btn btn-default text-center"
                                        type="button"
                                        onClick={this.handleStudentGenerateUsername}
                                    >
                                        Generiši korisničko ime
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className='col-lg-6 col-md-6 col-sm-6'>
                            <div className='text-align-center'>
                                <h4>Unesite podatke o roditelju</h4>
                                <br/>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Ime'
                                        type='text'
                                        name='firstName'
                                        placeholder='Unesi ime'
                                        value={this.state.fields.parent.firstName}
                                        onChange={this.onParentChange}
                                        validate={Validator.validateFirstName}
                                        error={this.state.parentFieldErrors.firstName}
                                        errorMessage='Ime mora imati između 3 i 20 slova i započinjati velikim slovom'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Prezime'
                                        type='text'
                                        name='lastName'
                                        placeholder='Unesi prezime'
                                        value={this.state.fields.parent.lastName}
                                        onChange={this.onParentChange}
                                        validate={Validator.validateLastName}
                                        error={this.state.parentFieldErrors.lastName}
                                        errorMessage='Prezime mora imati između 3 i 20 slova i započinjati velikim slovom'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor='birthdate'>Datum rođenja</label>
                                    <Datetime
                                        inputProps={{
                                            placeholder: 'Odaberi datum',
                                            style: {borderColor: this.state.parentFieldErrors.birthdate ? 'red' : null}
                                        }}
                                        timeFormat={false}
                                        name='birthdate'
                                        onChange={this.onParentBirthdateChange}
                                        value={moment(this.state.fields.parent.birthdate)}
                                    />
                                    <span style={{color: 'red'}}>{this.state.parentFieldErrors.birthdate}</span>
                                    <br/>
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Adresa'
                                        type='text'
                                        name='address'
                                        placeholder='Unesi adresu'
                                        value={this.state.fields.parent.address}
                                        onChange={this.onParentChange}
                                        validate={Validator.validateAddress}
                                        error={this.state.parentFieldErrors.address}
                                        errorMessage='Adresa mora imati najmanje 3 slova'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Jedinstveni matični broj građanina'
                                        type='text'
                                        name='umcn'
                                        placeholder='Unesi JMBG'
                                        value={this.state.fields.parent.umcn}
                                        onChange={this.onParentChange}
                                        validate={Validator.validateUmcn}
                                        error={this.state.parentFieldErrors.umcn}
                                        errorMessage='JMBG mora imati 13 cifara'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Telefon'
                                        type='text'
                                        name='phone'
                                        placeholder='Unesi broj telefona'
                                        value={this.state.fields.parent.phone}
                                        onChange={this.onParentChange}
                                        validate={Validator.validatePhone}
                                        error={this.state.parentFieldErrors.phone}
                                        errorMessage='Neispravan telefon'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor='umcn'>Generisano korisničko ime</label>
                                    <input
                                        type='text'
                                        name='username'
                                        className='form-control'
                                        readOnly={true}
                                        value={this.state.fields.parent.username}
                                        style={{backgroundColor: '#f7f9fc'}}
                                    />
                                    <br/>
                                </div>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor="phone"> Spol </label>
                                    <select
                                        name='sex'
                                        className='form-control'
                                        placeholder='Odaberi spol'
                                        value={this.state.fields.parent.sex}
                                        onChange={this.onParentSexChange}
                                    >
                                        <option value='M'>Muški</option>
                                        <option value='F'>Ženski</option>
                                    </select>
                                    <br/>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Lozinka'
                                        type='password'
                                        name='password'
                                        placeholder='Unesi lozinku'
                                        value={this.state.fields.parent.password}
                                        onChange={this.onParentChange}
                                        validate={Validator.validatePassword}
                                        error={this.state.parentFieldErrors.password}
                                        errorMessage='Lozinka mora imati više od 6 znakova'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor="confirmPassword"> Potvrdi lozinku </label>
                                    <input
                                        type='password'
                                        name='confirmPassword'
                                        className='form-control'
                                        placeholder='potvrdi lozinku'
                                        value={this.state.fields.parent.confirmPassword}
                                        onChange={this.onParentConfirmPasswordChange}
                                    />
                                    <br/>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='col form-group col-lg-12 text-center'>
                                    <button
                                        className="btn btn-default text-center"
                                        type="button"
                                        onClick={this.handleParentGenerateUsername}
                                    >
                                        Generiši korisničko ime
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className='form-row'>
                        <div className='col-lg-10 col-lg-offset-1 form-group'>
                            {
                                this.state.messages.errorMessage &&
                                <Alert
                                    onClick={this.clearFormMessage}
                                    type='alert-danger'
                                    strong='Problem!'
                                    message={this.state.messages.errorMessage}
                                />
                            }
                            {
                                this.state.messages.successMessage &&
                                <Alert
                                    onClick={this.clearFormMessage}
                                    type='alert-success'
                                    strong='Uspjeh!'
                                    message={this.state.messages.successMessage}
                                />
                            }
                        </div>
                    </div>
                    <div className='row'>
                        <br/>
                        <div className='col form-group col-lg-12 text-center'>
                            <button
                                className="btn btn-primary text-center"
                                type="button"
                                onClick={this.onRegisterStudent}
                            >
                                Registruj
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default RegisterStudent;