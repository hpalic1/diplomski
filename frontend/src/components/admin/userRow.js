import React from 'react';
import personFemale from '../../images/person-female.png';
import personMale from '../../images/person-male.png';
import schoolTeacher from '../../images/school-teacher.png';

class UserRow extends React.Component {
    render() {
        let image = null;
        switch (this.props.userType) {
            case 'student':
            case 'parent':
                if (this.props.user.sex === 'M') {
                    image = personMale;
                } else {
                    image = personFemale;
                }
                break;
            case 'teacher':
                image = schoolTeacher;
                break;
            default:
                console.warn('default image, fix it');
                image = personMale;
        }
        return (
            <tr className="cursor-pointer">
                <td className='first-column'><h4>{this.props.index + 1}</h4></td>
                <td onClick={(e) => this.props.onClick(this.props.user._id, this.props.userType, this.props.user)}
                >
                    <img className='profile-image' src={image} alt='no img'/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {this.props.user.firstName.concat(' ', this.props.user.lastName)}
                </td>
            </tr>
        );
    }
}

export default UserRow;