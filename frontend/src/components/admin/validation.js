export const Validator = {
    validateDateFormat(date) {
        const today = new Date();
        if (date >= today)
            return false;
        return typeof date !== 'string';
    },
    validateFirstName(firstName) {
        if (!firstName)
            return false;
        if (!(firstName[0] === firstName[0].toLocaleUpperCase()))
            return false;
        return firstName.length >= 3 && firstName.length <= 20;
    },
    validateLastName(lastName) {
        if (!lastName)
            return false;
        if (!(lastName[0] === lastName[0].toLocaleUpperCase()))
            return false;
        return lastName.length >= 3 && lastName.length <= 20;
    },
    validateAddress(address) {
        return address.length >= 3;
    },
    validateUmcn(umcn) {
        const regex = /^[0-9]/;
        if (!regex.test(umcn))
            return false;
        return umcn.length === 13;
    },
    validatePhone(phone) {
        return true;
    },
    validatePassword(password) {
        return password.length >= 6;
    },
    validateSubjectName(name) {
        if (!name)
            return false;
        if (!(name[0] === name[0].toLocaleUpperCase()))
            return false;
        return name.length >= 3 && name.length <= 30;
    },
    validateForm(fields, fieldErrors) {
        const errorMessages = Object.keys(fieldErrors).filter((k) => fieldErrors[k]);

        if (!fields.firstName
            || !fields.lastName
            || !fields.confirmPassword
            || !fields.password
            || !fields.umcn
            || !fields.address
            || !fields.birthdate
            || !fields.phone
            || !fields.sex
            || !fields.username) {
            return false;
        }

        if (errorMessages.length) {
            return false;
        }
        return true;
    },
    validateSubjectForm(fields, fieldErrors) {
        const errorMessages = Object.keys(fieldErrors).filter((k) => fieldErrors[k]);

        if (!fields.name
            || !fields.description
            || !fields.literature
            || !fields.numberOfHours) {
            return false;
        }

        if (errorMessages.length) {
            return false;
        }
        return true;
    }
};