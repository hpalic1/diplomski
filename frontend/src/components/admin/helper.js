import {countUsersWithSameLastName} from './actions';

export function generateUsername(firstName, lastName) {
    return new Promise((resolve, reject) => {

        let username = firstName[0].toLocaleLowerCase() + lastName.toLocaleLowerCase();
        countUsersWithSameLastName(lastName)
            .then((number) => {
                username += (number + 1).toString();
                resolve(username);
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });
    });
}

export function resetFields() {
    return {
        firstName: '',
        lastName: '',
        birthdate: '',
        address: '',
        umcn: '',
        phone: '',
        username: '',
        sex: '',
        password: '',
        confirmPassword: '',
    }
}