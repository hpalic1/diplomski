import React from 'react';
import {Modal} from "react-bootstrap";
import UserRow from "./userRow";
import uuid from "uuid";

export default class ClassBookModal extends React.Component {
    render() {
        let teacher = this.props.classBookModal.teacher ? this.props.classBookModal.teacher.firstName + ' ' + this.props.classBookModal.teacher.lastName : '';
        return (
            <Modal show={this.props.classBookModal.show} className='working-hour-modal'>
                <div className='modal-content'>
                    <div className="modal-header">
                        <h3 className="modal-title text-align-center">{`Razrednik: ${teacher}`}</h3>
                    </div>
                    <div className='modal-body'>
                        <table className='table table-hover'>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Učenici</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.props.classBookModal.students.map((student, index) => {
                                    return (
                                        <UserRow key={uuid()}
                                                 index={index}
                                                 user={student}
                                                 userType='student'
                                                 onClick={() => {

                                                 }}
                                        />
                                    );
                                })
                            }
                            </tbody>
                        </table>
                        <br/>
                        <div className='text-center'>
                            <button
                                className="btn btn-primary text-center"
                                type="button"
                                onClick={() => {
                                    this.props.onClassBookModalClose();
                                }}
                            >
                                Zatvori
                            </button>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}