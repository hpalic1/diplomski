import {baseURL} from "../../config";
import helpers from '../../helpers/storage.helper';

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    let error = new Error(`Http Error ${response.statusText}`);
    error.status = response.status;
    error.respones = response;
    console.log(error);
    throw error;
}

export function registerTeacher(data) {
    return new Promise(function (resolve, reject) {
        let message = {
            successMessage: '',
            errorMessage: '',
        };
        fetch(`${baseURL}/teacher`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(() => {
                message.successMessage = 'Uspješno registrovan nastavnik';
                resolve(message);
            }).catch(err => {
            message.errorMessage = 'Desila se greška';
            reject(message);
        });
    });
}

export function registerStudent(data) {
    return new Promise(function (resolve, reject) {
        let message = {
            successMessage: '',
            errorMessage: '',
        };
        fetch(`${baseURL}/student`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(() => {
                message.successMessage = 'Uspješno registrovan student';
                resolve(message);
            }).catch(err => {
            message.errorMessage = 'Desila se greška';
            reject(message);
        });
    });
}

export function registerParent(data) {
    return new Promise(function (resolve, reject) {
        let message = {
            successMessage: '',
            errorMessage: '',
        };
        fetch(`${baseURL}/parent`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then((res) => {
                message.successMessage = 'Uspješno registrovan roditelj';
                message.appendedParentId = res.data._id;
                resolve(message);
            }).catch(err => {
            message.errorMessage = 'Desila se greška';
            reject(message);
        });
    });
}

export function countUsersWithSameLastName(lastName) {
    return new Promise(function (resolve, reject) {
        let result = 0;
        fetch(`${baseURL}/teacher/${lastName}`, {
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then((res) => {
                result += res.length;
                fetch(`${baseURL}/student/${lastName}`, {
                    headers: new Headers({
                        'content-type': 'application/json',
                        'Authorization': `Bearer ${helpers.getAccesToken()}`
                    })
                }).then(checkStatus)
                    .then(res => res.json())
                    .then((res) => {
                        result += res.length;
                        fetch(`${baseURL}/parent/${lastName}`, {
                            headers: new Headers({
                                'content-type': 'application/json',
                                'Authorization': `Bearer ${helpers.getAccesToken()}`
                            })
                        }).then(checkStatus)
                            .then(res => res.json())
                            .then((res) => {
                                result += res.length;
                                resolve(result);
                            })
                            .catch(err => {
                                console.log(err);
                                reject(err);
                            });
                    })
                    .catch((err => {
                        console.log(err);
                        reject(err);
                    }));
            })
            .catch(err => {
                console.error(err);
                reject(err);
            });
    });
}

// userType can be 'student', 'teacher', 'parent'
export const getAllUsers = (userType) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/${userType}`, {
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log('getAllUsers', err);
                reject(err);
            })
    });
};

export const getAllStudentsByClassBook = (classBookId) => {
    let message = {
        successMessage: '',
        errorMessage: '',
    };
    return new Promise((resolve, reject) => {
        fetch(`${baseURL}/student/classBook/${classBookId}`, {
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                message.errorMessage = 'Desila se greška';
                reject(message);
            })
    });
};

export const editUser = (data, userType) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/${userType}`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};

export const addSubject = (data) => {
    return new Promise(function (resolve, reject) {
        let message = {
            successMessage: '',
            errorMessage: '',
        };
        fetch(`${baseURL}/subject`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(() => {
                message.successMessage = 'Uspješno dodan predmet';
                resolve(message);
            }).catch(err => {
            message.errorMessage = 'Desila se greška';
            reject(message);
        });
    });
};

export const getAllSubjects = () => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/subject`, {
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};

export const editSubject = (data) => {
    return new Promise(function (resolve, reject) {
        fetch(`${baseURL}/subject`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
    });
};


export const getUserById = (userType, userId) => {
    let message = {
        successMessage: '',
        errorMessage: '',
    };

    return new Promise((resolve, reject) => {
        fetch(`${baseURL}/${userType}/id/${userId}`, {
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization': `Bearer ${helpers.getAccesToken()}`
            })
        }).then(checkStatus)
            .then(res => res.json())
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                message.errorMessage = 'Desila se greška';
                reject(message);
            })
    });
};