import React from 'react';
import {Validator} from "./validation";
import {registerTeacher} from "./actions";
import InputField from '../inputField/inputField';
import {generateUsername, resetFields} from "./helper";

import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
import 'moment/locale/bs';
import Messages from "../messages/messages";

class RegisterTeacher extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                firstName: '',
                lastName: '',
                birthdate: '',
                address: '',
                umcn: '',
                phone: '',
                username: '',
                sex: 'M',
                password: '',
                confirmPassword: '',
            },
            messages: {},
            fieldErrors: {}
        };
    }

    onChange = (name, value, error) => { // Must use onChange like this or InputField component won't work

        const fields = Object.assign({}, this.state.fields);
        const fieldErrors = Object.assign({}, this.state.fieldErrors);

        fields[name] = value;
        fieldErrors[name] = error;
        this.setState({
            fields,
            fieldErrors,
        });
    };

    onBirthdateChange = (date) => {
        // date je objekat tima Moment
        const fieldErrors = Object.assign({}, this.state.fieldErrors);
        if (!Validator.validateDateFormat(date)) {
            fieldErrors.birthdate = 'Neispravan datum';
        } else {
            fieldErrors.birthdate = '';
        }
        let fields = this.state.fields;
        fields.birthdate = date;
        this.setState({
            fields,
            fieldErrors,
        });
    };

    onSexChange = (evt) => {
        let fields = Object.assign({}, this.state.fields);
        fields.sex = evt.target.value;
        this.setState({
            fields,
        });
    };

    onConfirmPasswordChange = (e) => {
        let fields = Object.assign({}, this.state.fields);
        fields.confirmPassword = e.target.value;
        this.setState({
            fields,
        })
    };

    onRegisterTeacher = (evt) => {
        evt.preventDefault();

        let messages = {};

        if (!Validator.validateForm(this.state.fields, this.state.fieldErrors)) {
            messages.errorMessage = 'Forma nije validna, ispravite greške i pokušajte ponovo';
            this.setState({
                messages,
            });
            return;
        }

        if (this.state.fields.password !== this.state.fields.confirmPassword) {
            messages.errorMessage = 'Lozinke se ne podudaraju';
            this.setState({
                messages,
            });
            return;
        }

        let data = JSON.parse(JSON.stringify(this.state.fields));

        delete data.confirmPassword;

        let promise = registerTeacher(data);
        promise.then(message => this.setState({
            messages: message,
        }))
            .then(() => {
                let emptyFields = resetFields();
                this.setState({
                    fields: emptyFields,
                });
            })
            .catch(message => this.setState({
                messages: message,
            }));
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    handleGenerateUsername = (e) => {
        e.preventDefault();

        let messages = {};

        if (!this.state.fields.firstName || !this.state.fields.lastName) {
            messages.errorMessage = 'Ime i prezime ne može biti prazno';
            this.setState({
                messages,
            });
            return;
        }

        generateUsername(this.state.fields.firstName, this.state.fields.lastName)
            .then(u => {
                const fields = this.state.fields;
                fields.username = u;
                this.setState({
                    fields,
                });
            })
            .catch(err => {
                console.error('Error generating username', err);
                messages.errorMessage = 'Greška pri kreiranju korisničkog imena';
                this.setState({
                    messages,
                })
            });
    };

    render() {
        return (
            <div className='container'>
                <form>
                    <div className='form-row'>
                        <div className='form-group col-lg-6'>
                            <InputField
                                label='Ime'
                                type='text'
                                name='firstName'
                                placeholder='Unesi ime'
                                value={this.state.fields.firstName}
                                onChange={this.onChange}
                                validate={Validator.validateFirstName}
                                error={this.state.fieldErrors.firstName}
                                errorMessage='Ime mora imati između 3 i 20 slova i započinjati velikim slovom'
                            />
                        </div>
                        <div className='form-group col-lg-6'>
                            <InputField
                                label='Prezime'
                                type='text'
                                name='lastName'
                                placeholder='Unesi prezime'
                                value={this.state.fields.lastName}
                                onChange={this.onChange}
                                validate={Validator.validateLastName}
                                error={this.state.fieldErrors.lastName}
                                errorMessage='Prezime mora imati između 3 i 20 slova i započinjati velikim slovom'
                            />
                        </div>
                    </div>
                    <div className='form-row'>
                        <div className='form-group col-lg-6'>
                            <label htmlFor='birthdate'>Datum rođenja</label>
                            <Datetime
                                inputProps={{
                                    placeholder: 'Odaberi datum',
                                    style: {borderColor: this.state.fieldErrors.birthdate ? 'red' : null}
                                }}
                                timeFormat={false}
                                name='birthdate'
                                onChange={this.onBirthdateChange}
                                value={this.state.fields.birthdate}
                            />
                            <span style={{color: 'red'}}>{this.state.fieldErrors.birthdate}</span>
                            <br/>
                        </div>
                        <div className='form-group col-lg-6'>
                            <InputField
                                label='Adresa'
                                type='text'
                                name='address'
                                placeholder='Unesi adresu'
                                value={this.state.fields.address}
                                onChange={this.onChange}
                                validate={Validator.validateAddress}
                                error={this.state.fieldErrors.address}
                                errorMessage='Adresa mora imati najmanje 3 slova'
                            />
                        </div>
                    </div>
                    <div className='form-row'>
                        <div className='form-group col-lg-6'>
                            <InputField
                                label='Jedinstveni matični broj građanina'
                                type='text'
                                name='umcn'
                                placeholder='Unesi JMBG'
                                value={this.state.fields.umcn}
                                onChange={this.onChange}
                                validate={Validator.validateUmcn}
                                error={this.state.fieldErrors.umcn}
                                errorMessage='JMBG mora imati 13 cifara'
                            />
                        </div>
                        <div className='form-group col-lg-6'>
                            <InputField
                                label='Telefon'
                                type='text'
                                name='phone'
                                placeholder='Unesi broj telefona'
                                value={this.state.fields.phone}
                                onChange={this.onChange}
                                validate={Validator.validatePhone}
                                error={this.state.fieldErrors.phone}
                                errorMessage='Neispravan telefon'
                            />
                        </div>
                    </div>
                    <div className='form-row'>
                        <div className='form-group col-lg-6'>
                            <label htmlFor='umcn'>Generisano korisničko ime</label>
                            <input
                                type='text'
                                name='username'
                                className='form-control'
                                readOnly={true}
                                value={this.state.fields.username}
                                style={{backgroundColor: '#f7f9fc'}}
                            />
                            <br/>
                        </div>
                        <div className='form-group col-lg-6'>
                            <label htmlFor="phone"> Spol </label>
                            <select
                                name='sex'
                                className='form-control'
                                placeholder='Odaberi spol'
                                value={this.state.fields.sex}
                                onChange={this.onSexChange}
                            >
                                <option value='M'>Muški</option>
                                <option value='F'>Ženski</option>
                            </select>
                            <br/>
                        </div>
                    </div>
                    <div className='form-row'>
                        <div className='form-group col-lg-6'>
                            <InputField
                                label='Lozinka'
                                type='password'
                                name='password'
                                placeholder='Unesi lozinku'
                                value={this.state.fields.password}
                                onChange={this.onChange}
                                validate={Validator.validatePassword}
                                error={this.state.fieldErrors.password}
                                errorMessage='Lozinka mora imati više od 6 znakova'
                            />
                        </div>
                        <div className='form-group col-lg-6'>
                            <label htmlFor="confirmPassword"> Potvrdi lozinku </label>
                            <input
                                type='password'
                                name='confirmPassword'
                                className='form-control'
                                placeholder='potvrdi lozinku'
                                value={this.state.fields.confirmPassword}
                                onChange={this.onConfirmPasswordChange}
                            />
                            <br/>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col form-group col-lg-6 text-center'>
                            <button
                                className="btn btn-default text-center"
                                type="button"
                                onClick={this.handleGenerateUsername}
                            >
                                Generiši korisničko ime
                            </button>
                        </div>
                        <div className='col form-group col-lg-6 text-center'>
                            <button
                                className="btn btn-primary text-center"
                                type="button"
                                onClick={this.onRegisterTeacher}
                            >
                                Registruj
                            </button>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-lg-10 col-lg-offset-1 form-group'>
                            <Messages
                                errorMessage={this.state.messages.errorMessage}
                                successMessage={this.state.messages.successMessage}
                                clearFormMessage={this.clearFormMessage}
                            />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default RegisterTeacher;