import React from 'react';
import uuid from "uuid";
import {getAllStudentsByClassBook, getAllUsers} from "./actions";
import NumericInput from "react-numeric-input";
import constants from '../../config/classConstants.config';
import ClassBookRow from "../teacher/classBookRow";
import Alert from "../alert/alert";
import {addClassBook} from "../teacher/actions";
import ClassBookModal from "./classBookModal";

export default class CreateClassBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                grade: '',
                class: '',
            },
            teachers: [],
            classBooks: [],
            selectedTeacherId: null,
            selectedTeacher: '',
            loaders: {
                classBookLoading: true,
            },
            message: {},
            classBookModal: {
                show: false,
                students: [],
            }
        };
    }

    componentDidMount() {
        getAllUsers('teacher')
            .then((teachers) => {
                this.setState({
                    teachers: teachers
                }, () => {
                    if (this.state.teachers.length !== 0) {
                        this.setState({
                            selectedTeacherId: this.state.teachers[0]._id,
                            selectedTeacher: this.state.teachers[0].firstName + ' ' + this.state.teachers[0].lastName,
                        })
                    }
                })
            }).catch(err => {
            console.error(err);
        }).then(() => {
            getAllUsers('classBook')
                .then((classBooks) => {
                    this.setState({
                        classBooks,
                        loaders: {
                            classBookLoading: false,
                        }
                    })
                }).catch(err => {
                console.error(err);
            });
        });
    }

    onSelectTeacherChange = (e) => {
        this.setState({
            selectedTeacherId: this.state.teachers[e.nativeEvent.target.selectedIndex]._id,
            selectedTeacher: this.state.teachers[e.nativeEvent.target.selectedIndex].firstName.concat(' ', this.state.teachers[e.nativeEvent.target.selectedIndex].lastName)
        })
    };

    handleGradeChange = (value) => {
        const fields = JSON.parse(JSON.stringify(this.state.fields));
        fields.grade = value;
        this.setState({
            fields,
        })
    };

    handleClassChange = (value) => {
        const fields = JSON.parse(JSON.stringify(this.state.fields));
        fields.class = value;
        this.setState({
            fields,
        })
    };

    clearMessage = (e) => {
        e.preventDefault();
        this.setState({
            message: {}
        })
    };

    onClassBookModalClose = () => {
        this.setState({
            classBookModal: {
                show: false,
                students: [],
                teacher: null,
            }
        })
    };

    handleClassBookRowClick = (classBook) => {
        getAllStudentsByClassBook(classBook._id)
            .then(students => {
                let teacher = null;
                this.state.teachers.forEach(t => {
                    if (t._id === classBook.teacherId)
                        teacher = t;
                });
                this.setState({
                    classBookModal: {
                        show: !this.state.classBookModal.show,
                        students: students,
                        teacher: teacher,
                    }
                })
            })
    };

    onAddClassBook = (e) => {
        if (this.state.fields.grade === '' || this.state.fields.class === '' || this.state.selectedTeacherId === null) {
            this.setState({
                message: {
                    errorMessage: 'Unesite podatke',
                }
            });
        } else {
            let data = JSON.parse(JSON.stringify(this.state.fields));
            data.teacherId = this.state.selectedTeacherId;

            let teacherHasClassBook = false;

            this.state.classBooks.forEach(classBook => {
                if (classBook.teacherId === data.teacherId) {
                    this.setState({
                        message: {
                            errorMessage: 'Taj nastavnik već ima odjeljensku knjigu',
                        }
                    });
                    teacherHasClassBook = true;
                }
            });

            if (teacherHasClassBook) return;

            addClassBook(data)
                .then(message => {
                    getAllUsers('classBook')
                        .then((classBooks) => {
                            this.setState({
                                classBooks,
                                loaders: {
                                    classBookLoading: false,
                                },
                                message: message,
                                fields: {
                                    grade: '',
                                    class: '',
                                },
                            })
                        }).catch(err => {
                        console.error(err);
                    });
                })
                .catch(message => {
                    this.setState({
                        message: message
                    });
                })
        }
    };

    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-6 col-md-6 col-xs-6'>
                        <div className='row'>
                            <div className='form-group col-lg-6 col-md-6 col-xs-6'>
                                <label>Razred</label>
                                <NumericInput
                                    className='form-control'
                                    name='grade'
                                    min={1}
                                    max={constants.maxGrade}
                                    value={this.state.fields.grade}
                                    strict
                                    onChange={(e) => this.handleGradeChange(e)}
                                />
                            </div>
                            <div className='form-group col-lg-6 col-md-6 col-xs-6'>
                                <label>Odjeljenje</label>
                                <NumericInput
                                    className='form-control'
                                    name='class'
                                    min={1}
                                    max={constants.maxClass}
                                    value={this.state.fields.class}
                                    strict
                                    onChange={(e) => this.handleClassChange(e)}
                                />
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col col-lg-12 col-md-12 col-xs-12'>
                                <label>Odaberi nastavnika</label>
                                <select className='form-control'
                                        onChange={(e) => this.onSelectTeacherChange(e)}
                                        value={this.state.selectedTeacher}
                                >
                                    {
                                        this.state.teachers.map((teacher) => {
                                            return (
                                                <option key={uuid()}>
                                                    {teacher.firstName.concat(' ', teacher.lastName)}
                                                </option>
                                            );
                                        })
                                    }
                                </select>
                                <br/>
                                <div className='col-lg-12 form-group'>
                                    {
                                        this.state.message.errorMessage &&
                                        <Alert
                                            onClick={this.clearMessage}
                                            type='alert-danger'
                                            strong='Problem!'
                                            message={this.state.message.errorMessage}
                                        />
                                    }
                                    {
                                        this.state.message.successMessage &&
                                        <Alert
                                            onClick={this.clearMessage}
                                            type='alert-success'
                                            strong='Uspjeh!'
                                            message={this.state.message.successMessage}
                                        />
                                    }
                                </div>
                                <div className='text-align-center'>
                                    <button
                                        className="btn btn-primary "
                                        type="button"
                                        onClick={(e) => this.onAddClassBook(e)}
                                    >
                                        Dodaj odjeljensku knjigu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-lg-1 col-md-1 col-xs-1'>
                    </div>
                    <div className='col-lg-5 col-md-5 col-xs-5'>
                        <label>Odjeljenske knjige</label>
                        {
                            this.state.loaders.classBookLoading ?
                                <div className='text-align-center'>
                                    <div className="loader-admin-tables"/>
                                </div> :
                                <table className='table table-hover'>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Razred</th>
                                        <th>Odjeljenje</th>
                                        <th>Razrednik</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.classBooks.map((classBook, index) => {
                                            return (
                                                <ClassBookRow key={uuid()}
                                                              index={index}
                                                              classBook={classBook}
                                                              onClick={(e) => this.handleClassBookRowClick(classBook)}
                                                />
                                            );
                                        })
                                    }
                                    </tbody>
                                </table>
                        }
                    </div>
                </div>
                <ClassBookModal
                    classBookModal={this.state.classBookModal}
                    onClassBookModalClose={this.onClassBookModalClose}
                />
            </div>
        );
    }

}