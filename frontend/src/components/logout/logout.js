import React from 'react';
import {Redirect} from "react-router-dom";

class Logout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            logoutInProgress: false,
            shouldRedirect: false,
        };

        setTimeout(() => {
            sessionStorage.clear();
            this.setState({
                logoutInProgress: false,
            }, () => {

            });
        }, 1000);

    }

    componentWillMount() {
        this.setState({
            logoutInProgress: true,
        });
    }

    render() {
        return (
            this.state.logoutInProgress ?
                <div className='text-align-center'>
                    <div className="loader-logout"/>
                </div> :
                <Redirect to='/login'>

                </Redirect>
        );
    }
}

export default Logout;