import React from 'react';
import Login from "../login/login";
import {Route} from "react-router-dom";

class MyRoute extends React.Component {

    render() {
        return (
            <Route exact path={this.props.path} render={() => {
                return (
                    <Login/>
                );
            }}/>
        );
    }
}

export default MyRoute;