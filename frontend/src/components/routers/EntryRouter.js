import React from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';

import Login from '../login/login';
import Navbar from "../navbar/navbar";
import MyRoute from "../myRoute/myRoute";
import Logout from "../logout/logout";

import helpers from "../../helpers/storage.helper";
import navBarLinks from '../../config/navBarLinks.config';

import RegisterTeacher from '../admin/registerTeacher';
import RegisterStudent from '../admin/registerStudent';
import Users from "../admin/users";
import Subjects from "../admin/subjects";
import Registry from "../teacher/registry";
import Directory from "../teacher/directory";
import Classbook from "../teacher/classbook";
import CreateClassBook from "../admin/createClassBook";
import WorkingDay from "../teacher/workingDay";
import StudentData from "../student/studentData";
import Student from "../student/student";
import Parent from "../parent/parent";
import ParentMeeting from "../teacher/parentMeeting";
import ParentMeetingList from "../parent/parentMeetingList";

const PrivateRoute = ({component: Component, ...rest}) => {

    let links = null;

    if (helpers.getCurrentUser()) {
        switch (helpers.getCurrentUser().role) {
            case 'admin':
                links = navBarLinks[0].routes;
                break;
            case 'teacher':
                links = navBarLinks[1].routes;
                break;
            case 'student':
                links = navBarLinks[2].routes;
                break;
            case 'parent':
                links = navBarLinks[3].routes;
                break;
            default:
                break;
        }
    }

    return (
        <Route {...rest} render={(props) => (
            helpers.getCurrentUser() ?
                <div>
                    <Navbar links={links}/>
                    <Component {...props}/>
                </div>
                : <Redirect
                    to={{
                        pathname: "/login",
                        state: {from: props.location}
                    }}
                />
        )}
        />
    )
};

const NoMatch = ({location}) => (
    <div>
        <h3>No match for <code>{location.pathname}</code></h3>
    </div>
);

class EntryRouter extends React.Component {
    render() {
        return (
            <Router>
                <Switch>
                    <MyRoute path='/login' component={Login}/>
                    <Route exact path='/' render={() => <Redirect to='/login'/>}/>
                    <Route exact path='/logout' component={Logout}/>}/>

                    <PrivateRoute exact path='/admin/registerTeacher' component={RegisterTeacher}/>
                    <PrivateRoute exact path='/admin/registerStudent' component={RegisterStudent}/>
                    <PrivateRoute exact path='/admin/users' component={Users}/>
                    <PrivateRoute exact path='/admin/subjects' component={Subjects}/>
                    <PrivateRoute exact path='/admin/classBooks' component={CreateClassBook}/>

                    <PrivateRoute exact path='/teacher/registry' component={Registry}/>
                    <PrivateRoute exact path='/teacher/directory' component={Directory}/>
                    <PrivateRoute exact path='/teacher/classbook' component={Classbook}/>
                    <PrivateRoute exact path='/teacher/workingDay' component={WorkingDay}/>
                    <PrivateRoute exact path='/teacher/parentMeeting' component={ParentMeeting}/>
                    <PrivateRoute exact path='/teacher/student/:studentId' component={StudentData}/>

                    <PrivateRoute exact path='/student' component={Student}/>

                    <PrivateRoute exact path='/parent' component={Parent}/>
                    <PrivateRoute exact path='/parent/parentMeeting' component={ParentMeetingList}/>

                    <Route component={NoMatch}/>
                </Switch>
            </Router>
        );
    }
}

export default EntryRouter;