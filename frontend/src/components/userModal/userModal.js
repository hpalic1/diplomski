import React from 'react';
import {Modal} from "react-bootstrap";
import InputField from "../inputField/inputField";
import {Validator} from "../admin/validation";
import Alert from "../alert/alert";
import Datetime from "react-datetime";
import {editUser, getUserById} from "../admin/actions";
import moment from "moment";
import Messages from "../messages/messages";

class UserModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                firstName: '',
                lastName: '',
                birthdate: '',
                address: '',
                umcn: '',
                phone: '',
                username: '',
                sex: '',
                password: '',
                confirmPassword: '',
            },
            fieldErrors: [],
            messages: [],
        }
    }


    componentWillReceiveProps(props) {
        if (Object.keys(props.userModal.user).length === 0) {
            return;
        }
        this.setState({
            fields: {
                firstName: props.userModal.user.firstName,
                lastName: props.userModal.user.lastName,
                birthdate: moment(props.userModal.user.birthdate),
                address: props.userModal.user.address,
                umcn: props.userModal.user.umcn,
                phone: props.userModal.user.phone,
                username: props.userModal.user.username,
                sex: props.userModal.user.sex,
                password: props.userModal.user.password,
                confirmPassword: props.userModal.user.password,
            },
            userId: props.userModal.id
        })
    }

    onChange = (name, value, error) => { // Must use onChange like this or InputField component won't work

        const fields = Object.assign({}, this.state.fields);
        const fieldErrors = Object.assign({}, this.state.fieldErrors);

        fields[name] = value;
        fieldErrors[name] = error;
        this.setState({
            fields,
            fieldErrors,
        });
    };

    onConfirmPasswordChange = (e) => {
        let fields = Object.assign({}, this.state.fields);
        fields.confirmPassword = e.target.value;
        this.setState({
            fields,
        })
    };

    onBirthdateChange = (date) => {
        // date je objekat tima Moment
        const fieldErrors = Object.assign({}, this.state.fieldErrors);
        if (!Validator.validateDateFormat(date)) {
            fieldErrors.birthdate = 'Neispravan datum';
        } else {
            fieldErrors.birthdate = '';
        }
        let fields = this.state.fields;
        fields.birthdate = date;
        this.setState({
            fields,
            fieldErrors,
        });
    };

    onSexChange = (evt) => {
        let fields = Object.assign({}, this.state.fields);
        fields.sex = evt.target.value;
        this.setState({
            fields,
        });
    };

    onEditButtonClick = () => {
        let messages = {};

        if (!Validator.validateForm(this.state.fields, this.state.fieldErrors)) {
            messages.errorMessage = 'Forma nije validna, ispravite greške i pokušajte ponovo';
            this.setState({
                messages,
            });
            return;
        }

        if (this.state.fields.password !== this.state.fields.confirmPassword) {
            messages.errorMessage = 'Lozinke se ne podudaraju';
            this.setState({
                messages,
            });
            return;
        }

        let changePassword = false;

        getUserById(this.props.userModal.userType, this.state.userId)
            .then(user => {
                if (user.password !== this.state.fields.password) {
                    changePassword = true;
                }
            })
            .then(() => {
                let id = this.state.userId;
                let data = JSON.parse(JSON.stringify(this.state.fields));
                data.id = id;
                data.changePassword = changePassword;

                editUser(data, this.props.userModal.userType)
                    .then(() => {
                        this.props.editSuccess(this.state.fields);
                    })
                    .catch(err => console.log(err))
            })
            .catch(err => console.error(err));
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    handleCloseModal = () => {
        this.setState({
            fieldErrors: [],
            messages: [],
        }, () => this.props.closeModal())
    };

    render() {
        return (
            <Modal show={this.props.userModal.show}>
                <div className='modal-content'>
                    <form>
                        <div className="modal-header">
                            <h3 className="modal-title text-align-center">{this.state.fields.username}</h3>
                        </div>
                        <div className='modal-body'>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Ime'
                                        type='text'
                                        name='firstName'
                                        placeholder='Unesi ime'
                                        value={this.state.fields.firstName}
                                        onChange={this.onChange}
                                        validate={Validator.validateFirstName}
                                        error={this.state.fieldErrors.firstName}
                                        errorMessage='Ime mora imati između 3 i 20 slova'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Prezime'
                                        type='text'
                                        name='lastName'
                                        placeholder='Unesi prezime'
                                        value={this.state.fields.lastName}
                                        onChange={this.onChange}
                                        validate={Validator.validateLastName}
                                        error={this.state.fieldErrors.lastName}
                                        errorMessage='Prezime mora imati između 3 i 20 slova'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor='birthdate'>Datum rođenja</label>
                                    <Datetime
                                        inputProps={{
                                            placeholder: 'Odaberi datum',
                                            style: {borderColor: this.state.fieldErrors.birthdate ? 'red' : null}
                                        }}
                                        timeFormat={false}
                                        name='birthdate'
                                        onChange={this.onBirthdateChange}
                                        value={this.state.fields.birthdate}
                                    />
                                    <span style={{color: 'red'}}>{this.state.fieldErrors.birthdate}</span>
                                    <br/>
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Adresa'
                                        type='text'
                                        name='address'
                                        placeholder='Unesi adresu'
                                        value={this.state.fields.address}
                                        onChange={this.onChange}
                                        validate={Validator.validateAddress}
                                        error={this.state.fieldErrors.address}
                                        errorMessage='Adresa mora imati najmanje 3 slova'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='JMBG'
                                        type='text'
                                        name='umcn'
                                        placeholder='Unesi JMBG'
                                        value={this.state.fields.umcn}
                                        onChange={this.onChange}
                                        validate={Validator.validateUmcn}
                                        error={this.state.fieldErrors.umcn}
                                        errorMessage='JMBG mora imati 13 cifara'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Telefon'
                                        type='text'
                                        name='phone'
                                        placeholder='Unesi broj telefona'
                                        value={this.state.fields.phone}
                                        onChange={this.onChange}
                                        validate={Validator.validatePhone}
                                        error={this.state.fieldErrors.phone}
                                        errorMessage='Neispravan telefon'
                                    />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor='umcn'>Generisano korisničko ime</label>
                                    <input
                                        type='text'
                                        name='username'
                                        className='form-control'
                                        readOnly={true}
                                        value={this.state.fields.username}
                                        style={{backgroundColor: '#f7f9fc'}}
                                    />
                                    <br/>
                                </div>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor="phone"> Spol </label>
                                    <select
                                        name='sex'
                                        className='form-control'
                                        placeholder='Odaberi spol'
                                        value={this.state.fields.sex}
                                        onChange={this.onSexChange}
                                    >
                                        <option value='M'>Muški</option>
                                        <option value='F'>Ženski</option>
                                    </select>
                                    <br/>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-group col-lg-6'>
                                    <InputField
                                        label='Lozinka'
                                        type='password'
                                        name='password'
                                        placeholder='Unesi lozinku'
                                        value={this.state.fields.password}
                                        onChange={this.onChange}
                                        validate={Validator.validatePassword}
                                        error={this.state.fieldErrors.password}
                                        errorMessage='Lozinka mora imati više od 6 znakova'
                                    />
                                </div>
                                <div className='form-group col-lg-6'>
                                    <label htmlFor="confirmPassword"> Potvrdi lozinku </label>
                                    <input
                                        type='password'
                                        name='confirmPassword'
                                        className='form-control'
                                        placeholder='potvrdi lozinku'
                                        value={this.state.fields.confirmPassword}
                                        onChange={this.onConfirmPasswordChange}
                                    />
                                    <br/>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col form-group col-lg-6 text-center'>
                                    <button
                                        className="btn btn-danger text-center"
                                        type="button"
                                        onClick={(e) => this.handleCloseModal()}
                                    >
                                        Izađi
                                    </button>

                                </div>
                                <div className='col form-group col-lg-6 text-center'>
                                    <button
                                        className="btn btn-primary text-center"
                                        type="button"
                                        onClick={(e) => {
                                            this.onEditButtonClick(e)
                                        }}
                                    >
                                        Izmijeni
                                    </button>

                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-lg-10 col-lg-offset-1 form-group'>
                                    <Messages
                                        errorMessage={this.state.messages.errorMessage}
                                        successMessage={this.state.messages.successMessage}
                                        clearFormMessage={this.clearFormMessage}
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </Modal>
        );
    }
}

export default UserModal;