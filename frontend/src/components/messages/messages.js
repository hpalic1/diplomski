import React from 'react';
import Alert from "../alert/alert";

const Messages = ({errorMessage, successMessage, clearFormMessage}) => {
    return (
        <div>
            {
                errorMessage &&
                <Alert
                    onClick={clearFormMessage}
                    type='alert-danger'
                    strong='Problem!'
                    message={errorMessage}
                />
            }
            {
                successMessage &&
                <Alert
                    onClick={clearFormMessage}
                    type='alert-success'
                    strong='Uspjeh!'
                    message={successMessage}
                />
            }
        </div>
    );
};

export default Messages;