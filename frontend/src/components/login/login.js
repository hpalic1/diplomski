import React from 'react';
import {Redirect} from 'react-router-dom';
import InputField from '../inputField/inputField';
import {Validator} from './validation';
import Alert from "../alert/alert";
import helpers from '../../helpers/storage.helper';
import * as config from '../../config';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {
                username: '',
                password: '',
            },
            fieldErrors: {},
            loginInProgress: false,
            shouldRedirect: false,
            messages: {},
        };
    }

    onChange = (name, value, error) => { // Must use onChange like this or InputField component won't work

        const fields = Object.assign({}, this.state.fields);
        const fieldErrors = Object.assign({}, this.state.fieldErrors);

        fields[name] = value;
        fieldErrors[name] = error;
        this.setState({
            fields,
            fieldErrors,
        });

    };

    loginUser = (evt) => {
        evt.preventDefault();

        this.setState({
            loginInProgress: true,
            messages: {},
        });

        const messages = {};

        fetch(`${config.baseURL}/login`, {
            method: 'POST',
            headers: new Headers({
                'content-type': 'application/json',
                'accept': 'application/json',
            }),
            body: JSON.stringify({
                password: this.state.fields.password,
                username: this.state.fields.username,
            }),
        }).then(res => {
            if (res.status !== 200) {
                messages.errorMessage = 'Nemate pristup';
            }
            return res;
        })
            .then(res => res.json())
            .then((res) => {
                if (!('errorMessage' in messages)) {
                    if ('role' in res.user) {
                        messages.successMessage = 'Uspješno ulogovan';

                        let currentUser = {
                            username: this.state.fields.username,
                            role: res.user.role,
                            id: res.user.id,
                        };

                        helpers.setCurrentUser(currentUser);
                        helpers.setAccesToken(res.acces_token);

                        this.setState({
                            messages,
                        });
                        setTimeout(() => {
                            this.setState({
                                shouldRedirect: true,
                                loginInProgress: false,
                            });
                        }, 700);
                    } else {
                        messages.errorMessage = "Pogrešan username ili password";
                        setTimeout(() => {
                            this.setState({
                                loginInProgress: false,
                                messages,
                            });
                        }, 900);
                    }
                } else {
                    setTimeout(() => {
                        this.setState({
                            loginInProgress: false,
                            messages,
                        });
                    }, 900);
                }
            }).catch(err => console.error(err));
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    render() {
        if (this.state.shouldRedirect) {
            switch (helpers.getCurrentUser().role) {
                case 'admin':
                    return (
                        <Redirect to={{
                            pathname: '/admin/users',
                            state: {role: 'admin'}
                        }}/>
                    );
                case 'teacher':
                    return (
                        <Redirect to={{
                            pathname: '/teacher/classbook',
                            state: {role: 'teacher'}
                        }}/>
                    );
                case 'student':
                    return (
                        <Redirect to={{
                            pathname: '/student',
                            state: {role: 'student'}
                        }}/>
                    );
                case 'parent':
                    return (
                        <Redirect to={{
                            pathname: '/parent',
                            state: {role: 'parent'}
                        }}/>
                    );
                default:
                    return;
            }
        }
        return (
            <div>
                <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                        <h1 className="display text-align-center">Elektronski dnevnik</h1>
                    </div>
                </div>
                <div className='container'>
                    <br/> <br/> <br/> <br/> <br/> <br/>
                    <div className='row'>
                        <div className='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3'>
                            <form>
                                <div className='form-group'>
                                    <InputField
                                        label='Korisničko ime'
                                        type='text'
                                        name='username'
                                        placeholder='Unesi korisničko ime'
                                        value={this.state.fields.username}
                                        onChange={this.onChange}
                                        validate={Validator.validateUsername}
                                        error={this.state.fieldErrors.username}
                                        errorMessage='Unesite korisničko ime'
                                    />
                                </div>
                                <div className='form-group'>
                                    <InputField
                                        label='Lozinka'
                                        type='password'
                                        name='password'
                                        placeholder='Unesite lozinku'
                                        value={this.state.fields.password}
                                        onChange={this.onChange}
                                        validate={Validator.validatePassword}
                                        error={this.state.fieldErrors.password}
                                        errorMessage='Unesite password'
                                    />
                                </div>
                                <div className='row'>
                                    <div className='text-center'>
                                        {

                                            !this.state.loginInProgress ? <button
                                                    type="submit"
                                                    className="btn btn-primary"
                                                    onClick={this.loginUser}
                                                >Login
                                                </button> :
                                                <div className='text-align-center'>
                                                    <div className="loader"/>
                                                </div>
                                        }

                                    </div>
                                </div>
                                <div className='row'>
                                    <br/>

                                    <div className='col-lg-10 col-lg-offset-1 form-group'>
                                        {
                                            this.state.messages.errorMessage &&
                                            <Alert
                                                onClick={this.clearFormMessage}
                                                type='alert-danger'
                                                strong='Problem!'
                                                message={this.state.messages.errorMessage}
                                            />
                                        }
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Login;