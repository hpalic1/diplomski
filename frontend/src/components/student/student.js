import React from 'react';
import StudentData from "./studentData";
import helpers from "../../helpers/storage.helper";

export default class Student extends React.Component {
    render() {
        return (
            <StudentData studentId={helpers.getCurrentUser().id}/>
        );
    }

};