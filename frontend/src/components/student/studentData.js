import React from 'react';
import {getAllSubjects, getUserById} from "../admin/actions";
import personFemale from '../../images/person-female.png';
import personMale from '../../images/person-male.png';
import helpers from "../../helpers/storage.helper";
import NumericInput from "react-numeric-input";
import Alert from "../alert/alert";
import uuid from "uuid";
import {editBehavior, markStudent} from "../teacher/actions";

export default class StudentData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            student: null,
            parent: null,
            subjects: [],
            mark: null,
            messages: {
                successMessage: '',
                errorMessage: '',
            },
            selectedSubjectId: null,
            selectedSubject: '',
            note: '',
            semester: 1,
            finalGrade: false,
            behavior: [
                {
                    semester: 1,
                    value: 'Primjerno'
                },
                {
                    semester: 2,
                    value: 'Primjerno'
                }
            ],
        };
        if (helpers.getCurrentUser().role === 'student' || helpers.getCurrentUser().role === 'parent') {
            helpers.setCurrentStudentNumber(1);
        }
    }

    componentDidMount() {

        getUserById('student', helpers.getCurrentUser().role === 'teacher' ? this.props.match.params.studentId : this.props.studentId)
            .then(student => {
                getUserById('parent', student.parentId)
                    .then(parent => {
                        this.setState({
                            student: student,
                            parent: parent,
                            behavior: student.behavior.length ? student.behavior : [
                                {
                                    semester: 1,
                                    value: 'Primjerno'
                                },
                                {
                                    semester: 2,
                                    value: 'Primjerno'
                                }
                            ]
                        })
                    })
            })
            .catch(err => console.log(err));
        getAllSubjects()
            .then(subjects => {
                let selectedSubjectId = null;
                let selectedSubject = '';
                subjects.forEach(subject => {
                    if (subject.teacherId === helpers.getCurrentUser().id) {
                        selectedSubjectId = subject._id;
                        selectedSubject = subject.name;
                    }
                });
                this.setState({
                    subjects,
                    selectedSubjectId,
                    selectedSubject,
                })
            })
            .catch(err => console.log(err));
    }

    handleMarkChange = (value) => {
        this.setState({
            mark: value,
        })
    };

    clearFormMessage = (e) => {
        e.preventDefault();
        this.setState({
            messages: {}
        })
    };

    onSelectSubject = (e) => {
        this.setState({
            selectedSubjectId: this.state.subjects[e.nativeEvent.target.selectedIndex]._id,
            selectedSubject: this.state.subjects[e.nativeEvent.target.selectedIndex].name,
        })
    };

    onNoteChange = (e) => {
        this.setState({
            note: e.target.value,
        })
    };

    onSelectSemesterChange = (e) => {
        this.setState({
            semester: e.nativeEvent.target.selectedIndex + 1,
        })
    };

    onChangeCheckBox = (e) => {
        this.setState({
            finalGrade: !this.state.finalGrade,
        })
    };

    onSelectBehaviorChange = (e, semester) => {
        let behavior = JSON.parse(JSON.stringify(this.state.behavior));
        behavior[semester - 1].value = e.target.value;

        let data = {
            behavior,
            id: this.state.student._id,
        };

        editBehavior(data)
            .then(student => {
                this.setState({
                    behavior,
                    messages: {
                        successMessage: 'Uspješno izmijenjen status vladanja'
                    }
                });
            })
            .catch(err => console.log(err));
    };

    getAvarege = (semester) => {
        let average = 0;
        let counter = 0;
        this.state.student.marks.forEach(mark => {
            if (mark.semester === semester && !mark.finalGrade) {
                average += mark.value;
                counter++;
            }
        });
        return isNaN(average / counter) ? 'Nema ocijena' : Math.round((average / counter) * 100) / 100;
    };

    onMarkStudent = () => {
        let finalGrade = {
            finalGradeExist: false,
            finalGradeSemester: null,
        };
        this.state.student.marks.forEach(mark => {
            if (mark.finalGrade && mark.semester === this.state.semester && mark.subjectId === this.state.selectedSubjectId) {
                finalGrade.finalGradeExist = true;
                finalGrade.finalGradeSemester = mark.semester;
            }
        });
        if (finalGrade.finalGradeExist && (finalGrade.finalGradeSemester === this.state.semester) && !this.state.finalGrade) {
            this.setState({
                messages: {
                    errorMessage: 'Nije moguće unijeti ocjenu ako postoji zaključna ocjena'
                }
            });
            return;
        }

        if (!this.state.mark) {
            this.setState({
                messages: {
                    errorMessage: 'Nije unesena ocjena'
                }
            });
            return;
        }
        let data = {
            id: this.state.student._id,
            mark: {
                subjectId: this.state.selectedSubjectId,
                value: this.state.mark,
                note: this.state.note,
                date: new Date(),
                semester: this.state.semester,
                finalGrade: this.state.finalGrade,
            },
            editFinalGrade: finalGrade.finalGradeExist && this.state.finalGrade,
        };
        markStudent(data)
            .then(student => {
                this.setState({
                    student,
                    messages: {
                        successMessage: data.editFinalGrade ? 'Uspješno izmijenjena zaključna ocjena' : 'Uspješno ocijenjen učenik'
                    }
                })
            })
            .catch(err => console.log(err));
    };

    render() {
        let image = null;
        if (this.state.student) {
            switch (this.state.student.sex) {
                case 'M':
                    image = personMale;
                    break;
                case 'F':
                    image = personFemale;
                    break;
                default:
                    break;
            }
        }
        let student = this.state.student;

        let parent = this.state.parent;

        let teacherCanMark = true;
        this.state.subjects.forEach(subject => {
            if (subject._id === this.state.selectedSubjectId && subject.teacherId === helpers.getCurrentUser().id) {
                teacherCanMark = false;
            }
        });

        return (
            student ?
                <div className='container'>
                    <div className='row'>
                        <div className='col-lg-6 col-md-6'>
                            <div className='row'>
                                <div className='col-lg-5 col-md-5'>
                                    <img className='student-data-image' src={image} alt='no img'/>
                                </div>
                                <div className='col-lg-7 col-md-7'>
                                    <h3>
                                        {(helpers.getCurrentStudentNumber() ? helpers.getCurrentStudentNumber() + '. ' : '1. ') + this.state.student.firstName + ' ' + this.state.student.lastName}
                                    </h3>
                                    <h4>
                                        {'Ime roditelja: ' + parent.firstName + ' ' + parent.lastName}
                                    </h4>
                                    <h4>
                                        {'Datum rođenja: ' + new Date(student.birthdate).toLocaleDateString('sr')}
                                    </h4>
                                    <h4>
                                        {'Adresa: ' + student.address}
                                    </h4>
                                    <h4>
                                        {'Telefon: ' + student.phone}
                                    </h4>
                                </div>
                            </div>
                            {
                                // Don't show mark entry for student or parent
                                !(helpers.getCurrentUser().role !== 'teacher' || teacherCanMark) ?
                                    <div className='row'>
                                        <div className='row'>
                                            <div className='col-lg-5 col-md-5'>
                                                <label>Ocjena</label>
                                                <NumericInput
                                                    className='form-control'
                                                    name='mark'
                                                    min={1}
                                                    max={5}
                                                    value={this.state.mark}
                                                    strict
                                                    onChange={(value) => this.handleMarkChange(value)}
                                                />
                                                <input className="form-check-input"
                                                       type="checkbox"
                                                       value=""
                                                       onChange={(e) => this.onChangeCheckBox(e)}
                                                       id="defaultCheck1"/>
                                                <label className="form-check-label">
                                                    {' Zaključna ocjena'}
                                                </label>
                                            </div>
                                            <div className='col-lg-5 col-md-5 pull-right'>
                                                <label>Polugodište</label>
                                                <select className='form-control'
                                                        onChange={(e) => this.onSelectSemesterChange(e)}
                                                        value={this.state.semester + '. Polugodište'}
                                                >
                                                    <option>{'1. Polugodište'}</option>
                                                    <option>{'2. Polugodište'}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <br/>
                                        <div className='row'>
                                            <div className='col-lg-12 col-md-12'>
                                                <label>Zabilješka uz ocjenu</label>
                                                <textarea
                                                    className='form-control resize-none'
                                                    rows={5}
                                                    placeholder={'Nije obavezno'}
                                                    name='note'
                                                    disabled={this.state.finalGrade}
                                                    value={this.state.finalGrade ? '' : this.state.note}
                                                    onChange={(e) => this.onNoteChange(e)}
                                                >
                                </textarea>
                                            </div>
                                        </div>
                                        <br/>
                                        <div className='col-lg-10 col-lg-offset-1 form-group'>
                                            {
                                                this.state.messages.errorMessage &&
                                                <Alert
                                                    onClick={this.clearFormMessage}
                                                    type='alert-danger'
                                                    strong='Problem!'
                                                    message={this.state.messages.errorMessage}
                                                />
                                            }
                                            {
                                                this.state.messages.successMessage &&
                                                <Alert
                                                    onClick={this.clearFormMessage}
                                                    type='alert-success'
                                                    strong='Uspjeh!'
                                                    message={this.state.messages.successMessage}
                                                />
                                            }
                                        </div>
                                        <div className='col form-group col-lg-12 text-center'>
                                            <button
                                                className={this.state.finalGrade ? "btn btn-danger text-center" : "btn btn-primary text-center"}
                                                type="button"
                                                onClick={(e) => {
                                                    this.onMarkStudent(e)
                                                }}
                                            >
                                                {this.state.finalGrade ? 'Unesi zaključnu ocjenu' : 'Unesi ocjenu'}
                                            </button>
                                        </div>
                                    </div>
                                    : null
                            }
                            {
                                <div>
                                    <div className='row'>
                                        <div className='col-lg-12'>
                                            <h3>Vladanje</h3>
                                        </div>
                                        <div className='col-lg-6'>
                                            <label>1. Polugodište</label>
                                            <select className='form-control'
                                                    disabled={!helpers.getIsClassTeacherTrue()}
                                                    onChange={(e) => this.onSelectBehaviorChange(e, 1)}
                                                    value={this.state.behavior.length ? this.state.behavior[0].value : 'Primjerno'}
                                            >
                                                <option>{'Primjerno'}</option>
                                                <option>{'Vrlodobro'}</option>
                                                <option>{'Dobro'}</option>
                                                <option>{'Zadovoljava'}</option>
                                                <option className='color-bad'>{'Ne zadovoljava'}</option>
                                            </select>
                                        </div>
                                        <div className='col-lg-6'>
                                            <label>2. Polugodište</label>
                                            <select className='form-control'
                                                    disabled={!helpers.getIsClassTeacherTrue()}
                                                    onChange={(e) => this.onSelectBehaviorChange(e, 2)}
                                                    value={this.state.behavior.length ? this.state.behavior[1].value : 'Primjerno'}
                                            >
                                                <option>{'Primjerno'}</option>
                                                <option>{'Vrlodobro'}</option>
                                                <option>{'Dobro'}</option>
                                                <option>{'Zadovoljava'}</option>
                                                <option className='color-bad'>{'Ne zadovoljava'}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className='col-lg-12'>
                                            <div className='row'>
                                                <div className='col-lg-12'>
                                                    <h3>Prosjek</h3>
                                                </div>
                                                <div className='col-lg-6'>
                                                    <label>1. Polugodište</label>
                                                    <input
                                                        className='form-control cursor-default read-only-background'
                                                        readOnly
                                                        value={this.getAvarege(1)}
                                                    />
                                                </div>
                                                <div className='col-lg-6'>
                                                    <label>2. Polugodište</label>
                                                    <input
                                                        className='form-control cursor-default read-only-background'
                                                        readOnly
                                                        value={this.getAvarege(2)}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                        <div className='col-lg-6 col-md-6'>
                            <div className='row'>
                                <div className='col-lg-12'>
                                    <label>{helpers.getIsClassTeacherTrue() || helpers.getCurrentUser().role === 'student' || helpers.getCurrentUser().role === 'parent' ? 'Odaberi predmet' : 'Vaš predmet'}</label>
                                    <select className='form-control'
                                            disabled={!helpers.getIsClassTeacherTrue() && !(helpers.getCurrentUser().role === 'student' || helpers.getCurrentUser().role === 'parent')}
                                            onChange={(e) => this.onSelectSubject(e)}
                                            value={this.state.selectedSubject}
                                    >
                                        {
                                            this.state.subjects.map((subject) => {
                                                return (
                                                    <option key={uuid()}>
                                                        {subject.name}
                                                    </option>
                                                );
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-lg-6 text-align-center'>
                                    <h4>Prvo polugodište</h4>
                                </div>
                                <div className='col-lg-6 text-align-center'>
                                    <h4>Drugo polugodište</h4>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-lg-6'>
                                    <table className='table'>
                                        <thead>
                                        <tr>
                                            <th>Ocjena</th>
                                            <th>Datum</th>
                                            <th>Zabilješka</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.student.marks.map(mark => {
                                                if (mark.semester === 1 && mark.subjectId === this.state.selectedSubjectId && !mark.finalGrade) {
                                                    return (
                                                        <tr className='tt' key={uuid()}>
                                                            <td>{mark.value}</td>
                                                            <td>{new Date(mark.date).toLocaleDateString('sr')}</td>
                                                            <td><span>{mark.note}</span></td>
                                                        </tr>
                                                    );
                                                } else return null;
                                            })
                                        }
                                        {
                                            // Displaying final grade
                                            this.state.student.marks.map(mark => {
                                                if (mark.semester === 1 && mark.subjectId === this.state.selectedSubjectId && mark.finalGrade) {
                                                    return (
                                                        <tr className='tt' key={uuid()}>
                                                            <td>Zaključna ocjena</td>
                                                            <td colSpan='2'><h4>{mark.value}</h4></td>
                                                        </tr>
                                                    )
                                                } else return null;
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                                <div className='col-lg-6'>
                                    <table className='table'>
                                        <thead>
                                        <tr>
                                            <th>Ocjena</th>
                                            <th>Datum</th>
                                            <th>Zabilješka</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.state.student.marks.map(mark => {
                                                if (mark.semester === 2 && mark.subjectId === this.state.selectedSubjectId) {
                                                    return (
                                                        <tr key={uuid()}>
                                                            <td>{mark.value}</td>
                                                            <td>{new Date(mark.date).toLocaleDateString('sr')}</td>
                                                            <td>{mark.note}</td>
                                                        </tr>
                                                    );
                                                } else return null;
                                            })
                                        }
                                        {
                                            // Displaying final grade
                                            this.state.student.marks.map(mark => {
                                                if (mark.semester === 2 && mark.subjectId === this.state.selectedSubjectId && mark.finalGrade) {
                                                    return (
                                                        <tr className='tt' key={uuid()}>
                                                            <td>Zaključna ocjena</td>
                                                            <td colSpan='2'><h4>{mark.value}</h4></td>
                                                        </tr>
                                                    )
                                                } else return null;
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> : null
        );
    }
}
