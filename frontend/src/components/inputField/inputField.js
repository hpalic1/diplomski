import React from 'react';

class InputField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value,
            error: false,
        }
    }

    componentWillReceiveProps(update) {
        this.setState({
            value: update.value,
            error: update.error,
        })
    }

    onChange = (e) => {
        const name = this.props.name;
        const value = e.target.value;

        let error = '';
        if (!this.props.validate(value)) {
            error = this.props.errorMessage;
        }
        this.props.onChange(name, value, error);
    };

    render() {
        return (
            <div>
                <label> {this.props.label} </label>
                <input
                    type={this.props.type}
                    name={this.props.name}
                    className='form-control'
                    placeholder={this.props.placeholder}
                    value={this.state.value}
                    onChange={this.onChange}
                    style={this.state.error ? {borderColor: 'red'} : null}
                />
                <span className='text-error'>{this.state.error}</span>
                <br/>
            </div>
        );
    }
}

export default InputField;

/*
* props:
* label
* type
* name
* placeholder
* value
* onChange
* error
* errorMessage
*
* */