export default {
    getCurrentUser() {
        return JSON.parse(sessionStorage.getItem('current_user'));
    },
    setCurrentUser(user) {
        sessionStorage.removeItem('current_user');
        sessionStorage.setItem('current_user', JSON.stringify(user));
    },
    removeCurrentUser() {
        sessionStorage.removeItem("acces_token");
        sessionStorage.removeItem("current_user");
    },
    setAccesToken(acces_token) {
        sessionStorage.setItem('acces_token', JSON.stringify(acces_token));
    },
    getAccesToken() {
        return JSON.parse(sessionStorage.getItem('acces_token'));
    },
    setCurrentClassBook(classBook) {
        sessionStorage.removeItem('current_classBook');
        sessionStorage.setItem('current_classBook', JSON.stringify(classBook));
    },
    getCurrentClassBook() {
        return JSON.parse(sessionStorage.getItem('current_classBook'));
    },
    removeCurrentClassBook() {
        sessionStorage.removeItem('current_classBook');
    },
    setIsClassTeacherToTrue() {
        sessionStorage.setItem('isClassTeacher', JSON.stringify(true));
    },
    getIsClassTeacherTrue() {
        return JSON.parse(sessionStorage.getItem('isClassTeacher'));
    },
    removeIsClassTeacherTrue() {
        sessionStorage.removeItem('isClassTeacher');
    },
    setCurrentWorkingDay(workingDay) {
        sessionStorage.setItem('working_day', JSON.stringify(workingDay));
    },
    getCurrentWorkingDay() {
        return JSON.parse(sessionStorage.getItem('working_day'));
    },
    setCurrentWorkingWeekNumber(workingWeekNumber) {
        sessionStorage.setItem('working_week_number', workingWeekNumber);
    },
    getCurrentWorkingWeekNumber() {
        return JSON.parse(sessionStorage.getItem('working_week_number'));
    },
    setCurrentStudentNumber(number) {
        sessionStorage.setItem('current_student_number', number);
    },
    getCurrentStudentNumber() {
        return sessionStorage.getItem('current_student_number');
    }

};