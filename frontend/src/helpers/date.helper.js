export const addDays = (date, days) => {
    let newDate = new Date(date);
    newDate.setDate(newDate.getDate() + days);
    return newDate;
};

export const subtractDays = (date, days) => {
    let newDate = new Date(date);
    newDate.setDate(newDate.getDate() - days);
    return newDate;
};